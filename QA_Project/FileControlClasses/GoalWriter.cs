﻿using QA_Project.GoalClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace QA_Project.FileControlClasses
{
    class GoalWriter
    {
        // Used to write goals to a .goal binary file
        String path;
        public GoalWriter(String filePath)
        {
            // Takes a file as a parameter to write the goals to 
            if (filePath.IndexOfAny(Path.GetInvalidPathChars()) != -1)
            {
                throw new Exception("Invalid File Path");
            }
            this.path = filePath;
        }
        public void writeGoal(Goal goal, Boolean overwrite)
        {
            // Writes the goals to the designated file
            GoalList goals;
            // Overwrite parameter dictates weather to add the goal to the already existing list of goals or to overwrite the whole file.
            if(overwrite || !File.Exists(this.path))
            {
                goals = new GoalList();
                goals.Add(goal);
            }
            else
            {
                // When adding to the existing list of goals, we need to read in the current goals first
                GoalReader gr = new GoalReader(this.path);
                goals = gr.ReadGoals();
                goals.Add(goal);
            }
            var formatter = new BinaryFormatter();
            using (var stream = new MemoryStream())
            {
                // Saves goals to the file
                formatter.Serialize(stream, goals);
                FileStream fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None);
                fs.Write(stream.ToArray(), 0, stream.ToArray().Length);
                fs.Close();
                stream.Close();
            }
        }
    }
}
