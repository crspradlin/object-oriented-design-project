﻿using QA_Project.GoalClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_Project.FileControlClasses
{
    // Uses GoalReader and GoalWriter Objects to save and read the app data file which stores the main/core goals within the application
    class AppDataFileController
    {
        private String directory;
        private String path;
        private GoalReader reader;
        private GoalWriter writer;

        // Singleton pattern is used to protect the app data file from corruption
        private static AppDataFileController uniqueInstance;

        private AppDataFileController() 
        {
            this.directory = @"%appdata%/TrackingPlusPlus";
            this.path = @"%appdata%/TrackingPlusPlus/goals.dat";
            this.reader = new GoalReader(this.path);
            this.writer = new GoalWriter(this.path);
        }
        public static AppDataFileController getInstance()
        {
            if (uniqueInstance == null)
            {
                uniqueInstance = new AppDataFileController();
            }
            return uniqueInstance;
        }

        public GoalList loadFromAppData()
        {
            // Loads the current goals within the app data file
            try
            {
                // Determine whether the directory exists.
                if (!Directory.Exists(directory))
                {
                    DirectoryInfo di = Directory.CreateDirectory(directory);
                }
                return reader.ReadGoals();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return new GoalList();
        }
        public void saveToAppData(GoalList goals)
        {
            // Saves the goals currently within the application to the app data file.
            if(goals.Count > 0)
            {
                writer.writeGoal(goals.ElementAt(0), true);
                for (var i = 1; i < goals.Count; i++)
                {
                    writer.writeGoal(goals.ElementAt(i), false);
                }
            }
        }
    }
}
