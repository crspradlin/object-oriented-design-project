﻿using QA_Project.GoalClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace QA_Project.FileControlClasses
{
    class GoalReader
    {
        // Read a goal saved within a .goal binary file
        String path;
 
        public GoalReader(String filePath)
        {
            // Takes a file as a parameter to read goals from
            if(filePath.IndexOfAny(Path.GetInvalidPathChars()) != -1)
            {
                throw new Exception("Invalid File Path");
            }
            this.path = filePath;
        }
        public GoalList ReadGoals()
        {
            // Returns the goals stored within the file
            GoalList goals = new GoalList();
            var formatter = new BinaryFormatter();
            using (var stream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
            {
                stream.Position = 0;
                goals = (GoalList)formatter.Deserialize(stream);
                stream.Close();
            }
            return goals;
        }
    }
}
