using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QA_Project.GoalClasses.GoalTypes;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using QA_Project.PanelClasses;

namespace QA_Project
{
    public partial class Form : System.Windows.Forms.Form
    {
        private void mp_deleteGoalBtn_Click(object sender, EventArgs e)
        {
            // Handles deleting the specifed selected goal within the application
            int temp = mp_goalList.SelectedIndex;
            if (temp != -1)
            {
                this.mp_goalList.Items.RemoveAt(temp);
                this.goals.RemoveAt(temp);
                this.appData.saveToAppData(this.goals);
            }
        }


        private void mp_goalList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (mp_goalList.SelectedIndex < 0)
            {
                this.mp_deleteGoalBtn.Enabled = false;
                this.mp_editGoalBtn.Enabled = false;
                this.saveToolStripMenuItem.Enabled = false;
            }
            else
            {
                this.saveToolStripMenuItem.Enabled = true;
                this.mp_deleteGoalBtn.Enabled = true;
                this.mp_editGoalBtn.Enabled = true;
            }

        }

        private void mp_editGoalBtn_Click(object sender, EventArgs e)
        {
            cp_mp_editGoal(null);
        }

        private void cp_mp_editGoal(Goal editGoal)
        {
            // Setsup create panel form to look like an edit form in order to edit current goals
           
            int index = mp_goalList.SelectedIndex;

            this.setToVisible(this.panelSpradlin_createGoalPanel);

            cp_goBackBtn2.Visible = true;
            cp_goBackBtn.Visible = false;

            Goal goalToEdit;
            if (editGoal == null)
                goalToEdit = this.goals.ElementAt(index);
            else
                goalToEdit = editGoal;
            IBaseGoalType temp = goalToEdit.GetGoalControl();

            cp_goalNameTextBox.Text = goalToEdit.GetName();

            // Based on type of goal, show the corresponding panel to edit that goal
            if (goalToEdit.GetType() == typeof(ReminderGoal))
            {
                this.cp_goalTypeComboBox.SelectedIndex = 1;
                this.cp_setVisible(this.cp_reminderPanel);
                cp_rStartDate.Value = temp.GetStartDate();
                cp_rEditGoalButton.Visible = true;
                cp_rCreateGoalBtn.Visible = false;
                cp_rGoalDescription.Text = ((ReminderGoalType)temp).GetTasks().First().getDescription();
            }
            else if (goalToEdit.GetType() == typeof(TimedGoal))
            {
                this.cp_goalTypeComboBox.SelectedIndex = 0;
                this.cp_setVisible(this.cp_timedPanel);
                cp_tStartDate.Value = temp.GetStartDate();
                cp_tEndDate.Value = temp.GetEndDate();
                cp_tEditGoalButton.Visible = true;
                cp_tCreateGoalBtn.Visible = false;
                cp_tGoalDescription.Text = ((TimedGoalType)temp).GetTasks().First().getDescription();
            }

            else if (goalToEdit.GetType() == typeof(CustomGoal)) //Custom goal
            {
                this.cp_goalTypeComboBox.SelectedIndex = 3;
                this.cp_setVisible(this.cp_customPanel);
                cp_cEditGoalButton.Visible = true;
                cp_cCreateGoalBtn.Visible = false;
                cp_cTaskListBox.Items.Clear();

                foreach (Timer.Stage s in goalToEdit.GetGoalControl().GetTasks())
                {
                    cp_cTaskListBox.Items.Add(s.getDescription());
                }
                cp_cStartDate.Value = temp.GetStartDate();
                if (goalToEdit.HasUnlimitedTime())
                {
                    cp_cEndDate.Value = temp.GetStartDate().AddDays(1);
                    cp_cNoEndDateCheck.Checked = true;
                }
                else
                    cp_cEndDate.Value = temp.GetEndDate();
            }
            else //Staged Goal
            {
                this.cp_goalTypeComboBox.SelectedIndex = 2;
                this.cp_setVisible(this.cp_stagedPanel);
                cp_sEndDate.Value = temp.GetEndDate();
                cp_sStartDate.Value = temp.GetStartDate();
                cp_sEditGoalButton.Visible = true;
                cp_sTaskListBox.Items.Clear();
                cp_sCreateGoalBtn.Visible = false;

                foreach (String s in ((StagedGoalType)temp).GetStages())
                {
                    cp_sTaskListBox.Items.Add(s);

                }
            }


        }

        private void cp_goBackBtn2_Click(object sender, EventArgs e)
        {
            cp_goBackBtn2.Visible = false;
            cp_goBackBtn.Visible = true;
            
            cp_rEditGoalButton.Visible = false;
            cp_sEditGoalButton.Visible = false;
            cp_tEditGoalButton.Visible = false;
            cp_cEditGoalButton.Visible = false;
            cp_rCreateGoalBtn.Visible = true;
            cp_sCreateGoalBtn.Visible = true;
            cp_tCreateGoalBtn.Visible = true;
            cp_cCreateGoalBtn.Visible = true;

            cp_goalTypeComboBox.Visible = true;
            cp_clearFields();
   
            this.setToVisible(this.panelSpradlin_mainPanel);
        }

        private void cp_sAddTaskBtn_Clicked(object sender, EventArgs e)
        {
            string input = Interaction.InputBox("What is the description for your new task?",
                       "New Task",
                       "",
                       0,
                       0);
            this.cp_sTaskListBox.Items.Add(input);
        }

        // Button handlers for all edit buttons within the create goal panels
        private void cp_reditButtonClick(object sender, EventArgs e)
        {
            mp_deleteGoalBtn_Click(sender, e);
            cp_rCreateGoalBtn_Click(sender, e);
            cp_goBackBtn2_Click(sender, e);
        }

        private void cp_tEditButtonClick(object sender, EventArgs e)
        {
            mp_deleteGoalBtn_Click(sender, e);
            cp_tCreateGoalBtn_Click(sender, e);
            cp_goBackBtn2_Click(sender, e);
        }

        private void cp_sEditButtonClick(object sender, EventArgs e)
        {
            mp_deleteGoalBtn_Click(sender, e);
            cp_sCreateGoalBtn_Click(sender, e);
            cp_goBackBtn2_Click(sender, e);
        }

        private void cp_cEditButtonClick(object sender, EventArgs e)
        {
            mp_deleteGoalBtn_Click(sender, e);
            cp_cCreateGoalBtn_Click(sender, e);
            cp_goBackBtn2_Click(sender, e);
        }
    }
}
