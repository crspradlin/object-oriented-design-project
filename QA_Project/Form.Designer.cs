namespace QA_Project
{
    partial class Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelSpradlin_mainPanel = new System.Windows.Forms.Panel();
            this.mp_editGoalBtn = new System.Windows.Forms.Button();
            this.mp_deleteGoalBtn = new System.Windows.Forms.Button();
            this.mp_createNewGoalBtn = new System.Windows.Forms.Button();
            this.mp_goalListLbl = new System.Windows.Forms.Label();
            this.mp_goalList = new System.Windows.Forms.ListBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelSpradlin_viewGoal = new System.Windows.Forms.Panel();
            this.vp_ss_goalCompletedBtn = new System.Windows.Forms.Button();
            this.vp_ss_goalDescription = new System.Windows.Forms.TextBox();
            this.vp_ss_goalDescriptionLabel = new System.Windows.Forms.Label();
            this.vp_endDate = new System.Windows.Forms.Label();
            this.vp_startDate = new System.Windows.Forms.Label();
            this.vp_endDateLabel = new System.Windows.Forms.Label();
            this.vp_startDateLabel = new System.Windows.Forms.Label();
            this.vp_goalStatus = new System.Windows.Forms.Label();
            this.vp_goalStatusLabel = new System.Windows.Forms.Label();
            this.vp_goalName = new System.Windows.Forms.Label();
            this.vp_goalNameLabel = new System.Windows.Forms.Label();
            this.vp_ms_checkBoxLabel = new System.Windows.Forms.Label();
            this.vp_ms_checkBox = new System.Windows.Forms.CheckedListBox();
            this.vp_goBackBtn = new System.Windows.Forms.Button();
            this.panelBethely = new System.Windows.Forms.Panel();
            this.panelBraith = new System.Windows.Forms.Panel();
            this.panelSpradlin_createGoalPanel = new System.Windows.Forms.Panel();
            this.cp_goBackBtn2 = new System.Windows.Forms.Button();
            this.cp_goBackBtn = new System.Windows.Forms.Button();
            this.cp_goalNameTextBox = new System.Windows.Forms.TextBox();
            this.cp_goalNameLabel = new System.Windows.Forms.Label();
            this.cp_goalTypeLabel = new System.Windows.Forms.Label();
            this.cp_goalTypeComboBox = new System.Windows.Forms.ComboBox();
            this.cp_timedPanel = new System.Windows.Forms.Panel();
            this.cp_tEditGoalButton = new System.Windows.Forms.Button();
            this.cp_tGoalDescriptionLabel = new System.Windows.Forms.Label();
            this.cp_tGoalDescription = new System.Windows.Forms.TextBox();
            this.cp_tErrorMsgBox = new System.Windows.Forms.TextBox();
            this.cp_tCreateGoalBtn = new System.Windows.Forms.Button();
            this.cp_tEndDate = new System.Windows.Forms.DateTimePicker();
            this.cp_tEndDateLabel = new System.Windows.Forms.Label();
            this.cp_tStartDate = new System.Windows.Forms.DateTimePicker();
            this.cp_tStartDateLabel = new System.Windows.Forms.Label();
            this.cp_stagedPanel = new System.Windows.Forms.Panel();
            this.cp_sEditGoalButton = new System.Windows.Forms.Button();
            this.cp_sErrorMsgBox = new System.Windows.Forms.TextBox();
            this.cp_sCreateGoalBtn = new System.Windows.Forms.Button();
            this.cp_sTaskListLabel = new System.Windows.Forms.Label();
            this.cp_sDeleteTaskBtn = new System.Windows.Forms.Button();
            this.cp_sAddTaskBtn = new System.Windows.Forms.Button();
            this.cp_sTaskListBox = new System.Windows.Forms.ListBox();
            this.cp_sEndDate = new System.Windows.Forms.DateTimePicker();
            this.cp_sEndDateLabel = new System.Windows.Forms.Label();
            this.cp_sStartDate = new System.Windows.Forms.DateTimePicker();
            this.cp_sStartDateLabel = new System.Windows.Forms.Label();
            this.cp_reminderPanel = new System.Windows.Forms.Panel();
            this.cp_rEditGoalButton = new System.Windows.Forms.Button();
            this.cp_rGoalDescriptionLabel = new System.Windows.Forms.Label();
            this.cp_rGoalDescription = new System.Windows.Forms.TextBox();
            this.cp_rErrorMsgBox = new System.Windows.Forms.TextBox();
            this.cp_rCreateGoalBtn = new System.Windows.Forms.Button();
            this.cp_rStartDate = new System.Windows.Forms.DateTimePicker();
            this.cp_rStartDateLabel = new System.Windows.Forms.Label();
            this.cp_customPanel = new System.Windows.Forms.Panel();
            this.cp_cEditGoalButton = new System.Windows.Forms.Button();
            this.cp_cErrorMsgBox = new System.Windows.Forms.TextBox();
            this.cp_cCreateGoalBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cp_cRemoveTaskBtn = new System.Windows.Forms.Button();
            this.cp_cAddTaskBtn = new System.Windows.Forms.Button();
            this.cp_cTaskListBox = new System.Windows.Forms.ListBox();
            this.cp_cNoEndDateCheck = new System.Windows.Forms.CheckBox();
            this.cp_cEndDate = new System.Windows.Forms.DateTimePicker();
            this.cp_cEndDateLabel = new System.Windows.Forms.Label();
            this.cp_cStartDate = new System.Windows.Forms.DateTimePicker();
            this.cp_cStartDateLabel = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.panelSpradlin_mainPanel.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panelSpradlin_viewGoal.SuspendLayout();
            this.panelSpradlin_createGoalPanel.SuspendLayout();
            this.cp_timedPanel.SuspendLayout();
            this.cp_stagedPanel.SuspendLayout();
            this.cp_reminderPanel.SuspendLayout();
            this.cp_customPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelSpradlin_mainPanel
            // 
            this.panelSpradlin_mainPanel.Controls.Add(this.mp_editGoalBtn);
            this.panelSpradlin_mainPanel.Controls.Add(this.mp_deleteGoalBtn);
            this.panelSpradlin_mainPanel.Controls.Add(this.mp_createNewGoalBtn);
            this.panelSpradlin_mainPanel.Controls.Add(this.mp_goalListLbl);
            this.panelSpradlin_mainPanel.Controls.Add(this.mp_goalList);
            this.panelSpradlin_mainPanel.Controls.Add(this.menuStrip1);
            this.panelSpradlin_mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSpradlin_mainPanel.Font = new System.Drawing.Font("Courier New", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelSpradlin_mainPanel.Location = new System.Drawing.Point(0, 0);
            this.panelSpradlin_mainPanel.Margin = new System.Windows.Forms.Padding(2);
            this.panelSpradlin_mainPanel.Name = "panelSpradlin_mainPanel";
            this.panelSpradlin_mainPanel.Size = new System.Drawing.Size(1063, 586);
            this.panelSpradlin_mainPanel.TabIndex = 0;
            // 
            // mp_editGoalBtn
            // 
            this.mp_editGoalBtn.Enabled = false;
            this.mp_editGoalBtn.Location = new System.Drawing.Point(26, 293);
            this.mp_editGoalBtn.Name = "mp_editGoalBtn";
            this.mp_editGoalBtn.Size = new System.Drawing.Size(149, 65);
            this.mp_editGoalBtn.TabIndex = 4;
            this.mp_editGoalBtn.Text = "Edit Goal";
            this.mp_editGoalBtn.UseVisualStyleBackColor = true;
            this.mp_editGoalBtn.Click += new System.EventHandler(this.mp_editGoalBtn_Click);
            // 
            // mp_deleteGoalBtn
            // 
            this.mp_deleteGoalBtn.Enabled = false;
            this.mp_deleteGoalBtn.Location = new System.Drawing.Point(26, 188);
            this.mp_deleteGoalBtn.Name = "mp_deleteGoalBtn";
            this.mp_deleteGoalBtn.Size = new System.Drawing.Size(149, 65);
            this.mp_deleteGoalBtn.TabIndex = 3;
            this.mp_deleteGoalBtn.Text = "Delete Goal";
            this.mp_deleteGoalBtn.UseVisualStyleBackColor = true;
            this.mp_deleteGoalBtn.Click += new System.EventHandler(this.mp_deleteGoalBtn_Click);
            // 
            // mp_createNewGoalBtn
            // 
            this.mp_createNewGoalBtn.Location = new System.Drawing.Point(26, 73);
            this.mp_createNewGoalBtn.Name = "mp_createNewGoalBtn";
            this.mp_createNewGoalBtn.Size = new System.Drawing.Size(149, 65);
            this.mp_createNewGoalBtn.TabIndex = 2;
            this.mp_createNewGoalBtn.Text = "Create New Goal";
            this.mp_createNewGoalBtn.UseVisualStyleBackColor = true;
            this.mp_createNewGoalBtn.Click += new System.EventHandler(this.mp_createNewGoalBtn_Click);
            // 
            // mp_goalListLbl
            // 
            this.mp_goalListLbl.AutoSize = true;
            this.mp_goalListLbl.Font = new System.Drawing.Font("Courier New", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mp_goalListLbl.Location = new System.Drawing.Point(199, 50);
            this.mp_goalListLbl.Name = "mp_goalListLbl";
            this.mp_goalListLbl.Size = new System.Drawing.Size(759, 20);
            this.mp_goalListLbl.TabIndex = 1;
            this.mp_goalListLbl.Text = "Goal Name                                                          Date Due";
            // 
            // mp_goalList
            // 
            this.mp_goalList.Font = new System.Drawing.Font("Courier New", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mp_goalList.FormattingEnabled = true;
            this.mp_goalList.ItemHeight = 25;
            this.mp_goalList.Location = new System.Drawing.Point(196, 73);
            this.mp_goalList.Name = "mp_goalList";
            this.mp_goalList.Size = new System.Drawing.Size(843, 479);
            this.mp_goalList.TabIndex = 0;
            this.mp_goalList.SelectedIndexChanged += new System.EventHandler(this.mp_goalList_SelectedIndexChanged);
            this.mp_goalList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.mp_goalList_MouseDoubleClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1063, 28);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(46, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(128, 26);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Enabled = false;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(128, 26);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // panelSpradlin_viewGoal
            // 
            this.panelSpradlin_viewGoal.AutoScroll = true;
            this.panelSpradlin_viewGoal.Controls.Add(this.vp_ss_goalCompletedBtn);
            this.panelSpradlin_viewGoal.Controls.Add(this.vp_ss_goalDescription);
            this.panelSpradlin_viewGoal.Controls.Add(this.vp_ss_goalDescriptionLabel);
            this.panelSpradlin_viewGoal.Controls.Add(this.vp_endDate);
            this.panelSpradlin_viewGoal.Controls.Add(this.vp_startDate);
            this.panelSpradlin_viewGoal.Controls.Add(this.vp_endDateLabel);
            this.panelSpradlin_viewGoal.Controls.Add(this.vp_startDateLabel);
            this.panelSpradlin_viewGoal.Controls.Add(this.vp_goalStatus);
            this.panelSpradlin_viewGoal.Controls.Add(this.vp_goalStatusLabel);
            this.panelSpradlin_viewGoal.Controls.Add(this.vp_goalName);
            this.panelSpradlin_viewGoal.Controls.Add(this.vp_goalNameLabel);
            this.panelSpradlin_viewGoal.Controls.Add(this.vp_ms_checkBoxLabel);
            this.panelSpradlin_viewGoal.Controls.Add(this.vp_ms_checkBox);
            this.panelSpradlin_viewGoal.Controls.Add(this.vp_goBackBtn);
            this.panelSpradlin_viewGoal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSpradlin_viewGoal.Location = new System.Drawing.Point(0, 0);
            this.panelSpradlin_viewGoal.Margin = new System.Windows.Forms.Padding(2);
            this.panelSpradlin_viewGoal.Name = "panelSpradlin_viewGoal";
            this.panelSpradlin_viewGoal.Size = new System.Drawing.Size(1063, 586);
            this.panelSpradlin_viewGoal.TabIndex = 1;
            this.panelSpradlin_viewGoal.Visible = false;
            // 
            // vp_ss_goalCompletedBtn
            // 
            this.vp_ss_goalCompletedBtn.Font = new System.Drawing.Font("Courier New", 10F);
            this.vp_ss_goalCompletedBtn.Location = new System.Drawing.Point(402, 400);
            this.vp_ss_goalCompletedBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.vp_ss_goalCompletedBtn.Name = "vp_ss_goalCompletedBtn";
            this.vp_ss_goalCompletedBtn.Size = new System.Drawing.Size(305, 43);
            this.vp_ss_goalCompletedBtn.TabIndex = 21;
            this.vp_ss_goalCompletedBtn.Text = "Set Goal to Complete";
            this.vp_ss_goalCompletedBtn.UseVisualStyleBackColor = true;
            this.vp_ss_goalCompletedBtn.Click += new System.EventHandler(this.vp_ss_goalCompletedBtn_Click);
            // 
            // vp_ss_goalDescription
            // 
            this.vp_ss_goalDescription.BackColor = System.Drawing.SystemColors.Control;
            this.vp_ss_goalDescription.Font = new System.Drawing.Font("Courier New", 10F);
            this.vp_ss_goalDescription.Location = new System.Drawing.Point(242, 238);
            this.vp_ss_goalDescription.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.vp_ss_goalDescription.Multiline = true;
            this.vp_ss_goalDescription.Name = "vp_ss_goalDescription";
            this.vp_ss_goalDescription.Size = new System.Drawing.Size(609, 141);
            this.vp_ss_goalDescription.TabIndex = 20;
            // 
            // vp_ss_goalDescriptionLabel
            // 
            this.vp_ss_goalDescriptionLabel.AutoSize = true;
            this.vp_ss_goalDescriptionLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.vp_ss_goalDescriptionLabel.Font = new System.Drawing.Font("Courier New", 10F);
            this.vp_ss_goalDescriptionLabel.Location = new System.Drawing.Point(245, 214);
            this.vp_ss_goalDescriptionLabel.Name = "vp_ss_goalDescriptionLabel";
            this.vp_ss_goalDescriptionLabel.Size = new System.Drawing.Size(179, 20);
            this.vp_ss_goalDescriptionLabel.TabIndex = 19;
            this.vp_ss_goalDescriptionLabel.Text = "Goal Description:";
            // 
            // vp_endDate
            // 
            this.vp_endDate.AutoSize = true;
            this.vp_endDate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.vp_endDate.Font = new System.Drawing.Font("Courier New", 12F);
            this.vp_endDate.Location = new System.Drawing.Point(668, 139);
            this.vp_endDate.Name = "vp_endDate";
            this.vp_endDate.Size = new System.Drawing.Size(58, 22);
            this.vp_endDate.TabIndex = 18;
            this.vp_endDate.Text = "date";
            // 
            // vp_startDate
            // 
            this.vp_startDate.AutoSize = true;
            this.vp_startDate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.vp_startDate.Font = new System.Drawing.Font("Courier New", 12F);
            this.vp_startDate.Location = new System.Drawing.Point(668, 65);
            this.vp_startDate.Name = "vp_startDate";
            this.vp_startDate.Size = new System.Drawing.Size(58, 22);
            this.vp_startDate.TabIndex = 17;
            this.vp_startDate.Text = "date";
            // 
            // vp_endDateLabel
            // 
            this.vp_endDateLabel.AutoSize = true;
            this.vp_endDateLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.vp_endDateLabel.Font = new System.Drawing.Font("Courier New", 10F);
            this.vp_endDateLabel.Location = new System.Drawing.Point(668, 123);
            this.vp_endDateLabel.Name = "vp_endDateLabel";
            this.vp_endDateLabel.Size = new System.Drawing.Size(149, 20);
            this.vp_endDateLabel.TabIndex = 16;
            this.vp_endDateLabel.Text = "Goal End Date:";
            // 
            // vp_startDateLabel
            // 
            this.vp_startDateLabel.AutoSize = true;
            this.vp_startDateLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.vp_startDateLabel.Font = new System.Drawing.Font("Courier New", 10F);
            this.vp_startDateLabel.Location = new System.Drawing.Point(668, 49);
            this.vp_startDateLabel.Name = "vp_startDateLabel";
            this.vp_startDateLabel.Size = new System.Drawing.Size(169, 20);
            this.vp_startDateLabel.TabIndex = 15;
            this.vp_startDateLabel.Text = "Goal Start Date:";
            // 
            // vp_goalStatus
            // 
            this.vp_goalStatus.AutoSize = true;
            this.vp_goalStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.vp_goalStatus.Font = new System.Drawing.Font("Courier New", 12F);
            this.vp_goalStatus.Location = new System.Drawing.Point(185, 139);
            this.vp_goalStatus.Name = "vp_goalStatus";
            this.vp_goalStatus.Size = new System.Drawing.Size(82, 22);
            this.vp_goalStatus.TabIndex = 14;
            this.vp_goalStatus.Text = "status";
            // 
            // vp_goalStatusLabel
            // 
            this.vp_goalStatusLabel.AutoSize = true;
            this.vp_goalStatusLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.vp_goalStatusLabel.Font = new System.Drawing.Font("Courier New", 10F);
            this.vp_goalStatusLabel.Location = new System.Drawing.Point(185, 123);
            this.vp_goalStatusLabel.Name = "vp_goalStatusLabel";
            this.vp_goalStatusLabel.Size = new System.Drawing.Size(129, 20);
            this.vp_goalStatusLabel.TabIndex = 13;
            this.vp_goalStatusLabel.Text = "Goal Status:";
            // 
            // vp_goalName
            // 
            this.vp_goalName.AutoSize = true;
            this.vp_goalName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.vp_goalName.Font = new System.Drawing.Font("Courier New", 12F);
            this.vp_goalName.Location = new System.Drawing.Point(185, 65);
            this.vp_goalName.Name = "vp_goalName";
            this.vp_goalName.Size = new System.Drawing.Size(58, 22);
            this.vp_goalName.TabIndex = 12;
            this.vp_goalName.Text = "name";
            // 
            // vp_goalNameLabel
            // 
            this.vp_goalNameLabel.AutoSize = true;
            this.vp_goalNameLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.vp_goalNameLabel.Font = new System.Drawing.Font("Courier New", 10F);
            this.vp_goalNameLabel.Location = new System.Drawing.Point(185, 49);
            this.vp_goalNameLabel.Name = "vp_goalNameLabel";
            this.vp_goalNameLabel.Size = new System.Drawing.Size(109, 20);
            this.vp_goalNameLabel.TabIndex = 11;
            this.vp_goalNameLabel.Text = "Goal Name:";
            // 
            // vp_ms_checkBoxLabel
            // 
            this.vp_ms_checkBoxLabel.AutoSize = true;
            this.vp_ms_checkBoxLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.vp_ms_checkBoxLabel.Font = new System.Drawing.Font("Courier New", 10F);
            this.vp_ms_checkBoxLabel.Location = new System.Drawing.Point(243, 214);
            this.vp_ms_checkBoxLabel.Name = "vp_ms_checkBoxLabel";
            this.vp_ms_checkBoxLabel.Size = new System.Drawing.Size(339, 20);
            this.vp_ms_checkBoxLabel.TabIndex = 10;
            this.vp_ms_checkBoxLabel.Text = "Check the tasks you\'ve completed:";
            // 
            // vp_ms_checkBox
            // 
            this.vp_ms_checkBox.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.vp_ms_checkBox.Font = new System.Drawing.Font("Courier New", 10F);
            this.vp_ms_checkBox.FormattingEnabled = true;
            this.vp_ms_checkBox.Location = new System.Drawing.Point(242, 238);
            this.vp_ms_checkBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.vp_ms_checkBox.Name = "vp_ms_checkBox";
            this.vp_ms_checkBox.Size = new System.Drawing.Size(609, 256);
            this.vp_ms_checkBox.TabIndex = 9;
            this.vp_ms_checkBox.SelectedIndexChanged += new System.EventHandler(this.vp_ms_checkBox_SelectedIndexChanged);
            // 
            // vp_goBackBtn
            // 
            this.vp_goBackBtn.Font = new System.Drawing.Font("Courier New", 10F);
            this.vp_goBackBtn.Location = new System.Drawing.Point(24, 49);
            this.vp_goBackBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.vp_goBackBtn.Name = "vp_goBackBtn";
            this.vp_goBackBtn.Size = new System.Drawing.Size(117, 29);
            this.vp_goBackBtn.TabIndex = 8;
            this.vp_goBackBtn.Text = "Go Back";
            this.vp_goBackBtn.UseVisualStyleBackColor = true;
            this.vp_goBackBtn.Click += new System.EventHandler(this.vp_goBackBtn_Click);
            // 
            // panelBethely
            // 
            this.panelBethely.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBethely.Location = new System.Drawing.Point(0, 0);
            this.panelBethely.Margin = new System.Windows.Forms.Padding(2);
            this.panelBethely.Name = "panelBethely";
            this.panelBethely.Size = new System.Drawing.Size(1063, 586);
            this.panelBethely.TabIndex = 2;
            this.panelBethely.Visible = false;
            // 
            // panelBraith
            // 
            this.panelBraith.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBraith.Location = new System.Drawing.Point(0, 0);
            this.panelBraith.Margin = new System.Windows.Forms.Padding(2);
            this.panelBraith.Name = "panelBraith";
            this.panelBraith.Size = new System.Drawing.Size(1063, 586);
            this.panelBraith.TabIndex = 3;
            this.panelBraith.Visible = false;
            // 
            // panelSpradlin_createGoalPanel
            // 
            this.panelSpradlin_createGoalPanel.AllowDrop = true;
            this.panelSpradlin_createGoalPanel.Controls.Add(this.cp_goBackBtn2);
            this.panelSpradlin_createGoalPanel.Controls.Add(this.cp_goBackBtn);
            this.panelSpradlin_createGoalPanel.Controls.Add(this.cp_goalNameTextBox);
            this.panelSpradlin_createGoalPanel.Controls.Add(this.cp_goalNameLabel);
            this.panelSpradlin_createGoalPanel.Controls.Add(this.cp_goalTypeLabel);
            this.panelSpradlin_createGoalPanel.Controls.Add(this.cp_goalTypeComboBox);
            this.panelSpradlin_createGoalPanel.Controls.Add(this.cp_customPanel);
            this.panelSpradlin_createGoalPanel.Controls.Add(this.cp_timedPanel);
            this.panelSpradlin_createGoalPanel.Controls.Add(this.cp_stagedPanel);
            this.panelSpradlin_createGoalPanel.Controls.Add(this.cp_reminderPanel);
            this.panelSpradlin_createGoalPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSpradlin_createGoalPanel.Location = new System.Drawing.Point(0, 0);
            this.panelSpradlin_createGoalPanel.Margin = new System.Windows.Forms.Padding(2);
            this.panelSpradlin_createGoalPanel.Name = "panelSpradlin_createGoalPanel";
            this.panelSpradlin_createGoalPanel.Size = new System.Drawing.Size(1063, 586);
            this.panelSpradlin_createGoalPanel.TabIndex = 4;
            this.panelSpradlin_createGoalPanel.Visible = false;
            // 
            // cp_goBackBtn2
            // 
            this.cp_goBackBtn2.Location = new System.Drawing.Point(27, 50);
            this.cp_goBackBtn2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cp_goBackBtn2.Name = "cp_goBackBtn2";
            this.cp_goBackBtn2.Size = new System.Drawing.Size(132, 36);
            this.cp_goBackBtn2.TabIndex = 9;
            this.cp_goBackBtn2.Text = "Go Back";
            this.cp_goBackBtn2.UseVisualStyleBackColor = true;
            this.cp_goBackBtn2.Visible = false;
            this.cp_goBackBtn2.Click += new System.EventHandler(this.cp_goBackBtn2_Click);
            // 
            // cp_goBackBtn
            // 
            this.cp_goBackBtn.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_goBackBtn.Location = new System.Drawing.Point(27, 50);
            this.cp_goBackBtn.Name = "cp_goBackBtn";
            this.cp_goBackBtn.Size = new System.Drawing.Size(132, 36);
            this.cp_goBackBtn.TabIndex = 7;
            this.cp_goBackBtn.Text = "Go Back";
            this.cp_goBackBtn.UseVisualStyleBackColor = true;
            this.cp_goBackBtn.Click += new System.EventHandler(this.cp_goBackBtn_Click);
            // 
            // cp_goalNameTextBox
            // 
            this.cp_goalNameTextBox.Font = new System.Drawing.Font("Courier New", 12F);
            this.cp_goalNameTextBox.Location = new System.Drawing.Point(556, 120);
            this.cp_goalNameTextBox.Name = "cp_goalNameTextBox";
            this.cp_goalNameTextBox.Size = new System.Drawing.Size(470, 30);
            this.cp_goalNameTextBox.TabIndex = 3;
            // 
            // cp_goalNameLabel
            // 
            this.cp_goalNameLabel.AutoSize = true;
            this.cp_goalNameLabel.Font = new System.Drawing.Font("Courier New", 12F);
            this.cp_goalNameLabel.Location = new System.Drawing.Point(421, 123);
            this.cp_goalNameLabel.Name = "cp_goalNameLabel";
            this.cp_goalNameLabel.Size = new System.Drawing.Size(118, 22);
            this.cp_goalNameLabel.TabIndex = 2;
            this.cp_goalNameLabel.Text = "Goal Name";
            // 
            // cp_goalTypeLabel
            // 
            this.cp_goalTypeLabel.AutoSize = true;
            this.cp_goalTypeLabel.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cp_goalTypeLabel.Location = new System.Drawing.Point(21, 123);
            this.cp_goalTypeLabel.Name = "cp_goalTypeLabel";
            this.cp_goalTypeLabel.Size = new System.Drawing.Size(118, 22);
            this.cp_goalTypeLabel.TabIndex = 1;
            this.cp_goalTypeLabel.Text = "Goal Type";
            // 
            // cp_goalTypeComboBox
            // 
            this.cp_goalTypeComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.cp_goalTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cp_goalTypeComboBox.Font = new System.Drawing.Font("Courier New", 12F);
            this.cp_goalTypeComboBox.FormattingEnabled = true;
            this.cp_goalTypeComboBox.Items.AddRange(new object[] {
            "Timed",
            "Reminder",
            "Multi Part",
            "Custom"});
            this.cp_goalTypeComboBox.Location = new System.Drawing.Point(157, 120);
            this.cp_goalTypeComboBox.Name = "cp_goalTypeComboBox";
            this.cp_goalTypeComboBox.Size = new System.Drawing.Size(230, 30);
            this.cp_goalTypeComboBox.TabIndex = 0;
            this.cp_goalTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.cp_goalTypeComboBox_SelectedIndexChanged);
            // 
            // cp_timedPanel
            // 
            this.cp_timedPanel.Controls.Add(this.cp_tEditGoalButton);
            this.cp_timedPanel.Controls.Add(this.cp_tGoalDescriptionLabel);
            this.cp_timedPanel.Controls.Add(this.cp_tGoalDescription);
            this.cp_timedPanel.Controls.Add(this.cp_tErrorMsgBox);
            this.cp_timedPanel.Controls.Add(this.cp_tCreateGoalBtn);
            this.cp_timedPanel.Controls.Add(this.cp_tEndDate);
            this.cp_timedPanel.Controls.Add(this.cp_tEndDateLabel);
            this.cp_timedPanel.Controls.Add(this.cp_tStartDate);
            this.cp_timedPanel.Controls.Add(this.cp_tStartDateLabel);
            this.cp_timedPanel.Location = new System.Drawing.Point(27, 170);
            this.cp_timedPanel.Name = "cp_timedPanel";
            this.cp_timedPanel.Size = new System.Drawing.Size(1001, 429);
            this.cp_timedPanel.TabIndex = 5;
            this.cp_timedPanel.Visible = false;
            // 
            // cp_tEditGoalButton
            // 
            this.cp_tEditGoalButton.Location = new System.Drawing.Point(680, 149);
            this.cp_tEditGoalButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cp_tEditGoalButton.Name = "cp_tEditGoalButton";
            this.cp_tEditGoalButton.Size = new System.Drawing.Size(171, 39);
            this.cp_tEditGoalButton.TabIndex = 28;
            this.cp_tEditGoalButton.Text = "Edit Goal";
            this.cp_tEditGoalButton.UseVisualStyleBackColor = true;
            this.cp_tEditGoalButton.Visible = false;
            this.cp_tEditGoalButton.Click += new System.EventHandler(this.cp_tEditButtonClick);
            // 
            // cp_tGoalDescriptionLabel
            // 
            this.cp_tGoalDescriptionLabel.AutoSize = true;
            this.cp_tGoalDescriptionLabel.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_tGoalDescriptionLabel.Location = new System.Drawing.Point(20, 21);
            this.cp_tGoalDescriptionLabel.Name = "cp_tGoalDescriptionLabel";
            this.cp_tGoalDescriptionLabel.Size = new System.Drawing.Size(169, 20);
            this.cp_tGoalDescriptionLabel.TabIndex = 27;
            this.cp_tGoalDescriptionLabel.Text = "Goal Description";
            // 
            // cp_tGoalDescription
            // 
            this.cp_tGoalDescription.Font = new System.Drawing.Font("Courier New", 12F);
            this.cp_tGoalDescription.Location = new System.Drawing.Point(24, 49);
            this.cp_tGoalDescription.Multiline = true;
            this.cp_tGoalDescription.Name = "cp_tGoalDescription";
            this.cp_tGoalDescription.Size = new System.Drawing.Size(470, 312);
            this.cp_tGoalDescription.TabIndex = 8;
            // 
            // cp_tErrorMsgBox
            // 
            this.cp_tErrorMsgBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.cp_tErrorMsgBox.Font = new System.Drawing.Font("Courier New", 12F);
            this.cp_tErrorMsgBox.ForeColor = System.Drawing.Color.Crimson;
            this.cp_tErrorMsgBox.Location = new System.Drawing.Point(566, 222);
            this.cp_tErrorMsgBox.Multiline = true;
            this.cp_tErrorMsgBox.Name = "cp_tErrorMsgBox";
            this.cp_tErrorMsgBox.ReadOnly = true;
            this.cp_tErrorMsgBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cp_tErrorMsgBox.Size = new System.Drawing.Size(394, 169);
            this.cp_tErrorMsgBox.TabIndex = 26;
            this.cp_tErrorMsgBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cp_tCreateGoalBtn
            // 
            this.cp_tCreateGoalBtn.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_tCreateGoalBtn.Location = new System.Drawing.Point(680, 149);
            this.cp_tCreateGoalBtn.Name = "cp_tCreateGoalBtn";
            this.cp_tCreateGoalBtn.Size = new System.Drawing.Size(171, 36);
            this.cp_tCreateGoalBtn.TabIndex = 17;
            this.cp_tCreateGoalBtn.Text = "Create Goal";
            this.cp_tCreateGoalBtn.UseVisualStyleBackColor = true;
            this.cp_tCreateGoalBtn.Click += new System.EventHandler(this.cp_tCreateGoalBtn_Click);
            // 
            // cp_tEndDate
            // 
            this.cp_tEndDate.CalendarFont = new System.Drawing.Font("Courier New", 12F);
            this.cp_tEndDate.CustomFormat = "MM/dd/yyyy hh:mm tt";
            this.cp_tEndDate.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_tEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.cp_tEndDate.Location = new System.Drawing.Point(693, 71);
            this.cp_tEndDate.Name = "cp_tEndDate";
            this.cp_tEndDate.Size = new System.Drawing.Size(267, 26);
            this.cp_tEndDate.TabIndex = 21;
            this.cp_tEndDate.Value = new System.DateTime(2020, 4, 12, 15, 46, 38, 0);
            // 
            // cp_tEndDateLabel
            // 
            this.cp_tEndDateLabel.AutoSize = true;
            this.cp_tEndDateLabel.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_tEndDateLabel.Location = new System.Drawing.Point(562, 76);
            this.cp_tEndDateLabel.Name = "cp_tEndDateLabel";
            this.cp_tEndDateLabel.Size = new System.Drawing.Size(89, 20);
            this.cp_tEndDateLabel.TabIndex = 20;
            this.cp_tEndDateLabel.Text = "End Date";
            // 
            // cp_tStartDate
            // 
            this.cp_tStartDate.CalendarFont = new System.Drawing.Font("Courier New", 12F);
            this.cp_tStartDate.CustomFormat = "MM/dd/yyyy hh:mm tt";
            this.cp_tStartDate.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_tStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.cp_tStartDate.Location = new System.Drawing.Point(693, 21);
            this.cp_tStartDate.Name = "cp_tStartDate";
            this.cp_tStartDate.Size = new System.Drawing.Size(267, 26);
            this.cp_tStartDate.TabIndex = 19;
            this.cp_tStartDate.Value = new System.DateTime(2020, 4, 12, 15, 46, 28, 0);
            // 
            // cp_tStartDateLabel
            // 
            this.cp_tStartDateLabel.AutoSize = true;
            this.cp_tStartDateLabel.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_tStartDateLabel.Location = new System.Drawing.Point(562, 26);
            this.cp_tStartDateLabel.Name = "cp_tStartDateLabel";
            this.cp_tStartDateLabel.Size = new System.Drawing.Size(109, 20);
            this.cp_tStartDateLabel.TabIndex = 16;
            this.cp_tStartDateLabel.Text = "Start Date";
            // 
            // cp_stagedPanel
            // 
            this.cp_stagedPanel.Controls.Add(this.cp_sEditGoalButton);
            this.cp_stagedPanel.Controls.Add(this.cp_sErrorMsgBox);
            this.cp_stagedPanel.Controls.Add(this.cp_sCreateGoalBtn);
            this.cp_stagedPanel.Controls.Add(this.cp_sTaskListLabel);
            this.cp_stagedPanel.Controls.Add(this.cp_sDeleteTaskBtn);
            this.cp_stagedPanel.Controls.Add(this.cp_sAddTaskBtn);
            this.cp_stagedPanel.Controls.Add(this.cp_sTaskListBox);
            this.cp_stagedPanel.Controls.Add(this.cp_sEndDate);
            this.cp_stagedPanel.Controls.Add(this.cp_sEndDateLabel);
            this.cp_stagedPanel.Controls.Add(this.cp_sStartDate);
            this.cp_stagedPanel.Controls.Add(this.cp_sStartDateLabel);
            this.cp_stagedPanel.Location = new System.Drawing.Point(27, 170);
            this.cp_stagedPanel.Name = "cp_stagedPanel";
            this.cp_stagedPanel.Size = new System.Drawing.Size(1001, 429);
            this.cp_stagedPanel.TabIndex = 4;
            this.cp_stagedPanel.Visible = false;
            // 
            // cp_sEditGoalButton
            // 
            this.cp_sEditGoalButton.Location = new System.Drawing.Point(689, 174);
            this.cp_sEditGoalButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cp_sEditGoalButton.Name = "cp_sEditGoalButton";
            this.cp_sEditGoalButton.Size = new System.Drawing.Size(171, 35);
            this.cp_sEditGoalButton.TabIndex = 27;
            this.cp_sEditGoalButton.Text = "Edit Goal";
            this.cp_sEditGoalButton.UseVisualStyleBackColor = true;
            this.cp_sEditGoalButton.Visible = false;
            this.cp_sEditGoalButton.Click += new System.EventHandler(this.cp_sEditButtonClick);
            // 
            // cp_sErrorMsgBox
            // 
            this.cp_sErrorMsgBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.cp_sErrorMsgBox.Font = new System.Drawing.Font("Courier New", 12F);
            this.cp_sErrorMsgBox.ForeColor = System.Drawing.Color.Crimson;
            this.cp_sErrorMsgBox.Location = new System.Drawing.Point(575, 230);
            this.cp_sErrorMsgBox.Multiline = true;
            this.cp_sErrorMsgBox.Name = "cp_sErrorMsgBox";
            this.cp_sErrorMsgBox.ReadOnly = true;
            this.cp_sErrorMsgBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cp_sErrorMsgBox.Size = new System.Drawing.Size(394, 169);
            this.cp_sErrorMsgBox.TabIndex = 26;
            this.cp_sErrorMsgBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cp_sCreateGoalBtn
            // 
            this.cp_sCreateGoalBtn.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_sCreateGoalBtn.Location = new System.Drawing.Point(689, 173);
            this.cp_sCreateGoalBtn.Name = "cp_sCreateGoalBtn";
            this.cp_sCreateGoalBtn.Size = new System.Drawing.Size(171, 36);
            this.cp_sCreateGoalBtn.TabIndex = 17;
            this.cp_sCreateGoalBtn.Text = "Create Goal";
            this.cp_sCreateGoalBtn.UseVisualStyleBackColor = true;
            this.cp_sCreateGoalBtn.Click += new System.EventHandler(this.cp_sCreateGoalBtn_Click);
            // 
            // cp_sTaskListLabel
            // 
            this.cp_sTaskListLabel.AutoSize = true;
            this.cp_sTaskListLabel.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_sTaskListLabel.Location = new System.Drawing.Point(32, 36);
            this.cp_sTaskListLabel.Name = "cp_sTaskListLabel";
            this.cp_sTaskListLabel.Size = new System.Drawing.Size(179, 20);
            this.cp_sTaskListLabel.TabIndex = 25;
            this.cp_sTaskListLabel.Text = "Tasks to Complete";
            // 
            // cp_sDeleteTaskBtn
            // 
            this.cp_sDeleteTaskBtn.Enabled = false;
            this.cp_sDeleteTaskBtn.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_sDeleteTaskBtn.Location = new System.Drawing.Point(185, 363);
            this.cp_sDeleteTaskBtn.Name = "cp_sDeleteTaskBtn";
            this.cp_sDeleteTaskBtn.Size = new System.Drawing.Size(163, 36);
            this.cp_sDeleteTaskBtn.TabIndex = 24;
            this.cp_sDeleteTaskBtn.Text = "Delete Task";
            this.cp_sDeleteTaskBtn.UseVisualStyleBackColor = true;
            // 
            // cp_sAddTaskBtn
            // 
            this.cp_sAddTaskBtn.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_sAddTaskBtn.Location = new System.Drawing.Point(36, 363);
            this.cp_sAddTaskBtn.Name = "cp_sAddTaskBtn";
            this.cp_sAddTaskBtn.Size = new System.Drawing.Size(119, 36);
            this.cp_sAddTaskBtn.TabIndex = 18;
            this.cp_sAddTaskBtn.Text = "Add Task";
            this.cp_sAddTaskBtn.UseVisualStyleBackColor = true;
            this.cp_sAddTaskBtn.Click += new System.EventHandler(this.cp_sAddTaskBtn_Click);
            // 
            // cp_sTaskListBox
            // 
            this.cp_sTaskListBox.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_sTaskListBox.FormattingEnabled = true;
            this.cp_sTaskListBox.ItemHeight = 20;
            this.cp_sTaskListBox.Location = new System.Drawing.Point(33, 79);
            this.cp_sTaskListBox.Name = "cp_sTaskListBox";
            this.cp_sTaskListBox.Size = new System.Drawing.Size(442, 244);
            this.cp_sTaskListBox.TabIndex = 23;
            // 
            // cp_sEndDate
            // 
            this.cp_sEndDate.CalendarFont = new System.Drawing.Font("Courier New", 12F);
            this.cp_sEndDate.CustomFormat = "MM/dd/yyyy hh:mm tt";
            this.cp_sEndDate.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_sEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.cp_sEndDate.Location = new System.Drawing.Point(702, 79);
            this.cp_sEndDate.Name = "cp_sEndDate";
            this.cp_sEndDate.Size = new System.Drawing.Size(267, 26);
            this.cp_sEndDate.TabIndex = 21;
            this.cp_sEndDate.Value = new System.DateTime(2020, 4, 12, 15, 46, 38, 0);
            // 
            // cp_sEndDateLabel
            // 
            this.cp_sEndDateLabel.AutoSize = true;
            this.cp_sEndDateLabel.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_sEndDateLabel.Location = new System.Drawing.Point(571, 84);
            this.cp_sEndDateLabel.Name = "cp_sEndDateLabel";
            this.cp_sEndDateLabel.Size = new System.Drawing.Size(89, 20);
            this.cp_sEndDateLabel.TabIndex = 20;
            this.cp_sEndDateLabel.Text = "End Date";
            // 
            // cp_sStartDate
            // 
            this.cp_sStartDate.CalendarFont = new System.Drawing.Font("Courier New", 12F);
            this.cp_sStartDate.CustomFormat = "MM/dd/yyyy hh:mm tt";
            this.cp_sStartDate.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_sStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.cp_sStartDate.Location = new System.Drawing.Point(702, 29);
            this.cp_sStartDate.Name = "cp_sStartDate";
            this.cp_sStartDate.Size = new System.Drawing.Size(267, 26);
            this.cp_sStartDate.TabIndex = 19;
            this.cp_sStartDate.Value = new System.DateTime(2020, 4, 12, 15, 46, 28, 0);
            // 
            // cp_sStartDateLabel
            // 
            this.cp_sStartDateLabel.AutoSize = true;
            this.cp_sStartDateLabel.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_sStartDateLabel.Location = new System.Drawing.Point(571, 34);
            this.cp_sStartDateLabel.Name = "cp_sStartDateLabel";
            this.cp_sStartDateLabel.Size = new System.Drawing.Size(109, 20);
            this.cp_sStartDateLabel.TabIndex = 16;
            this.cp_sStartDateLabel.Text = "Start Date";
            // 
            // cp_reminderPanel
            // 
            this.cp_reminderPanel.Controls.Add(this.cp_rEditGoalButton);
            this.cp_reminderPanel.Controls.Add(this.cp_rGoalDescriptionLabel);
            this.cp_reminderPanel.Controls.Add(this.cp_rGoalDescription);
            this.cp_reminderPanel.Controls.Add(this.cp_rErrorMsgBox);
            this.cp_reminderPanel.Controls.Add(this.cp_rCreateGoalBtn);
            this.cp_reminderPanel.Controls.Add(this.cp_rStartDate);
            this.cp_reminderPanel.Controls.Add(this.cp_rStartDateLabel);
            this.cp_reminderPanel.Location = new System.Drawing.Point(27, 170);
            this.cp_reminderPanel.Name = "cp_reminderPanel";
            this.cp_reminderPanel.Size = new System.Drawing.Size(1001, 429);
            this.cp_reminderPanel.TabIndex = 6;
            this.cp_reminderPanel.Visible = false;
            // 
            // cp_rEditGoalButton
            // 
            this.cp_rEditGoalButton.Location = new System.Drawing.Point(689, 75);
            this.cp_rEditGoalButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cp_rEditGoalButton.Name = "cp_rEditGoalButton";
            this.cp_rEditGoalButton.Size = new System.Drawing.Size(171, 37);
            this.cp_rEditGoalButton.TabIndex = 36;
            this.cp_rEditGoalButton.Text = "Edit Goal";
            this.cp_rEditGoalButton.UseVisualStyleBackColor = true;
            this.cp_rEditGoalButton.Visible = false;
            this.cp_rEditGoalButton.Click += new System.EventHandler(this.cp_reditButtonClick);
            // 
            // cp_rGoalDescriptionLabel
            // 
            this.cp_rGoalDescriptionLabel.AutoSize = true;
            this.cp_rGoalDescriptionLabel.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_rGoalDescriptionLabel.Location = new System.Drawing.Point(30, 29);
            this.cp_rGoalDescriptionLabel.Name = "cp_rGoalDescriptionLabel";
            this.cp_rGoalDescriptionLabel.Size = new System.Drawing.Size(279, 20);
            this.cp_rGoalDescriptionLabel.TabIndex = 35;
            this.cp_rGoalDescriptionLabel.Text = "Goal Description (Optional)";
            // 
            // cp_rGoalDescription
            // 
            this.cp_rGoalDescription.Font = new System.Drawing.Font("Courier New", 12F);
            this.cp_rGoalDescription.Location = new System.Drawing.Point(34, 57);
            this.cp_rGoalDescription.Multiline = true;
            this.cp_rGoalDescription.Name = "cp_rGoalDescription";
            this.cp_rGoalDescription.Size = new System.Drawing.Size(470, 312);
            this.cp_rGoalDescription.TabIndex = 28;
            // 
            // cp_rErrorMsgBox
            // 
            this.cp_rErrorMsgBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.cp_rErrorMsgBox.Font = new System.Drawing.Font("Courier New", 12F);
            this.cp_rErrorMsgBox.ForeColor = System.Drawing.Color.Crimson;
            this.cp_rErrorMsgBox.Location = new System.Drawing.Point(576, 165);
            this.cp_rErrorMsgBox.Multiline = true;
            this.cp_rErrorMsgBox.Name = "cp_rErrorMsgBox";
            this.cp_rErrorMsgBox.ReadOnly = true;
            this.cp_rErrorMsgBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cp_rErrorMsgBox.Size = new System.Drawing.Size(394, 169);
            this.cp_rErrorMsgBox.TabIndex = 34;
            this.cp_rErrorMsgBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cp_rCreateGoalBtn
            // 
            this.cp_rCreateGoalBtn.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_rCreateGoalBtn.Location = new System.Drawing.Point(689, 76);
            this.cp_rCreateGoalBtn.Name = "cp_rCreateGoalBtn";
            this.cp_rCreateGoalBtn.Size = new System.Drawing.Size(171, 36);
            this.cp_rCreateGoalBtn.TabIndex = 30;
            this.cp_rCreateGoalBtn.Text = "Create Goal";
            this.cp_rCreateGoalBtn.UseVisualStyleBackColor = true;
            this.cp_rCreateGoalBtn.Click += new System.EventHandler(this.cp_rCreateGoalBtn_Click);
            // 
            // cp_rStartDate
            // 
            this.cp_rStartDate.CalendarFont = new System.Drawing.Font("Courier New", 12F);
            this.cp_rStartDate.CustomFormat = "MM/dd/yyyy hh:mm tt";
            this.cp_rStartDate.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_rStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.cp_rStartDate.Location = new System.Drawing.Point(703, 29);
            this.cp_rStartDate.Name = "cp_rStartDate";
            this.cp_rStartDate.Size = new System.Drawing.Size(267, 26);
            this.cp_rStartDate.TabIndex = 31;
            this.cp_rStartDate.Value = new System.DateTime(2020, 4, 12, 15, 46, 28, 0);
            // 
            // cp_rStartDateLabel
            // 
            this.cp_rStartDateLabel.AutoSize = true;
            this.cp_rStartDateLabel.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_rStartDateLabel.Location = new System.Drawing.Point(572, 34);
            this.cp_rStartDateLabel.Name = "cp_rStartDateLabel";
            this.cp_rStartDateLabel.Size = new System.Drawing.Size(109, 20);
            this.cp_rStartDateLabel.TabIndex = 29;
            this.cp_rStartDateLabel.Text = "Start Date";
            // 
            // cp_customPanel
            // 
            this.cp_customPanel.Controls.Add(this.cp_cEditGoalButton);
            this.cp_customPanel.Controls.Add(this.cp_cErrorMsgBox);
            this.cp_customPanel.Controls.Add(this.cp_cCreateGoalBtn);
            this.cp_customPanel.Controls.Add(this.label1);
            this.cp_customPanel.Controls.Add(this.cp_cRemoveTaskBtn);
            this.cp_customPanel.Controls.Add(this.cp_cAddTaskBtn);
            this.cp_customPanel.Controls.Add(this.cp_cTaskListBox);
            this.cp_customPanel.Controls.Add(this.cp_cNoEndDateCheck);
            this.cp_customPanel.Controls.Add(this.cp_cEndDate);
            this.cp_customPanel.Controls.Add(this.cp_cEndDateLabel);
            this.cp_customPanel.Controls.Add(this.cp_cStartDate);
            this.cp_customPanel.Controls.Add(this.cp_cStartDateLabel);
            this.cp_customPanel.Location = new System.Drawing.Point(27, 170);
            this.cp_customPanel.Name = "cp_customPanel";
            this.cp_customPanel.Size = new System.Drawing.Size(1001, 429);
            this.cp_customPanel.TabIndex = 5;
            this.cp_customPanel.Visible = false;
            // 
            // cp_cEditGoalButton
            // 
            this.cp_cEditGoalButton.Location = new System.Drawing.Point(680, 165);
            this.cp_cEditGoalButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cp_cEditGoalButton.Name = "cp_cEditGoalButton";
            this.cp_cEditGoalButton.Size = new System.Drawing.Size(171, 36);
            this.cp_cEditGoalButton.TabIndex = 16;
            this.cp_cEditGoalButton.Text = "Edit Goal";
            this.cp_cEditGoalButton.UseVisualStyleBackColor = true;
            this.cp_cEditGoalButton.Visible = false;
            this.cp_cEditGoalButton.Click += new System.EventHandler(this.cp_cEditButtonClick);
            // 
            // cp_cErrorMsgBox
            // 
            this.cp_cErrorMsgBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.cp_cErrorMsgBox.Font = new System.Drawing.Font("Courier New", 12F);
            this.cp_cErrorMsgBox.ForeColor = System.Drawing.Color.Crimson;
            this.cp_cErrorMsgBox.Location = new System.Drawing.Point(566, 222);
            this.cp_cErrorMsgBox.Multiline = true;
            this.cp_cErrorMsgBox.Name = "cp_cErrorMsgBox";
            this.cp_cErrorMsgBox.ReadOnly = true;
            this.cp_cErrorMsgBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cp_cErrorMsgBox.Size = new System.Drawing.Size(394, 169);
            this.cp_cErrorMsgBox.TabIndex = 15;
            this.cp_cErrorMsgBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cp_cCreateGoalBtn
            // 
            this.cp_cCreateGoalBtn.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_cCreateGoalBtn.Location = new System.Drawing.Point(680, 165);
            this.cp_cCreateGoalBtn.Name = "cp_cCreateGoalBtn";
            this.cp_cCreateGoalBtn.Size = new System.Drawing.Size(171, 36);
            this.cp_cCreateGoalBtn.TabIndex = 8;
            this.cp_cCreateGoalBtn.Text = "Create Goal";
            this.cp_cCreateGoalBtn.UseVisualStyleBackColor = true;
            this.cp_cCreateGoalBtn.Click += new System.EventHandler(this.cp_cCreateGoalBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Courier New", 10F);
            this.label1.Location = new System.Drawing.Point(23, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 20);
            this.label1.TabIndex = 14;
            this.label1.Text = "Tasks to Complete";
            // 
            // cp_cRemoveTaskBtn
            // 
            this.cp_cRemoveTaskBtn.Enabled = false;
            this.cp_cRemoveTaskBtn.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_cRemoveTaskBtn.Location = new System.Drawing.Point(176, 355);
            this.cp_cRemoveTaskBtn.Name = "cp_cRemoveTaskBtn";
            this.cp_cRemoveTaskBtn.Size = new System.Drawing.Size(163, 36);
            this.cp_cRemoveTaskBtn.TabIndex = 13;
            this.cp_cRemoveTaskBtn.Text = "Delete Task";
            this.cp_cRemoveTaskBtn.UseVisualStyleBackColor = true;
            this.cp_cRemoveTaskBtn.Click += new System.EventHandler(this.cp_cRemoveTaskBtn_Click);
            // 
            // cp_cAddTaskBtn
            // 
            this.cp_cAddTaskBtn.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_cAddTaskBtn.Location = new System.Drawing.Point(27, 355);
            this.cp_cAddTaskBtn.Name = "cp_cAddTaskBtn";
            this.cp_cAddTaskBtn.Size = new System.Drawing.Size(119, 36);
            this.cp_cAddTaskBtn.TabIndex = 8;
            this.cp_cAddTaskBtn.Text = "Add Task";
            this.cp_cAddTaskBtn.UseVisualStyleBackColor = true;
            this.cp_cAddTaskBtn.Click += new System.EventHandler(this.cp_cAddTaskBtn_Click);
            // 
            // cp_cTaskListBox
            // 
            this.cp_cTaskListBox.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_cTaskListBox.FormattingEnabled = true;
            this.cp_cTaskListBox.ItemHeight = 20;
            this.cp_cTaskListBox.Location = new System.Drawing.Point(24, 71);
            this.cp_cTaskListBox.Name = "cp_cTaskListBox";
            this.cp_cTaskListBox.Size = new System.Drawing.Size(442, 244);
            this.cp_cTaskListBox.TabIndex = 12;
            this.cp_cTaskListBox.SelectedIndexChanged += new System.EventHandler(this.cp_cTaskListBox_SelectedIndexChanged);
            // 
            // cp_cNoEndDateCheck
            // 
            this.cp_cNoEndDateCheck.AutoSize = true;
            this.cp_cNoEndDateCheck.Font = new System.Drawing.Font("Courier New", 8F);
            this.cp_cNoEndDateCheck.Location = new System.Drawing.Point(693, 103);
            this.cp_cNoEndDateCheck.Name = "cp_cNoEndDateCheck";
            this.cp_cNoEndDateCheck.Size = new System.Drawing.Size(254, 21);
            this.cp_cNoEndDateCheck.TabIndex = 11;
            this.cp_cNoEndDateCheck.Text = "Set goal to have no end date";
            this.cp_cNoEndDateCheck.UseVisualStyleBackColor = true;
            this.cp_cNoEndDateCheck.CheckedChanged += new System.EventHandler(this.cp_cNoEndDateCheck_CheckedChanged);
            // 
            // cp_cEndDate
            // 
            this.cp_cEndDate.CalendarFont = new System.Drawing.Font("Courier New", 12F);
            this.cp_cEndDate.CustomFormat = "MM/dd/yyyy hh:mm tt";
            this.cp_cEndDate.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_cEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.cp_cEndDate.Location = new System.Drawing.Point(693, 71);
            this.cp_cEndDate.Name = "cp_cEndDate";
            this.cp_cEndDate.Size = new System.Drawing.Size(267, 26);
            this.cp_cEndDate.TabIndex = 10;
            this.cp_cEndDate.Value = new System.DateTime(2020, 4, 12, 15, 46, 38, 0);
            // 
            // cp_cEndDateLabel
            // 
            this.cp_cEndDateLabel.AutoSize = true;
            this.cp_cEndDateLabel.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_cEndDateLabel.Location = new System.Drawing.Point(562, 76);
            this.cp_cEndDateLabel.Name = "cp_cEndDateLabel";
            this.cp_cEndDateLabel.Size = new System.Drawing.Size(89, 20);
            this.cp_cEndDateLabel.TabIndex = 9;
            this.cp_cEndDateLabel.Text = "End Date";
            // 
            // cp_cStartDate
            // 
            this.cp_cStartDate.CalendarFont = new System.Drawing.Font("Courier New", 12F);
            this.cp_cStartDate.CustomFormat = "MM/dd/yyyy hh:mm tt";
            this.cp_cStartDate.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_cStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.cp_cStartDate.Location = new System.Drawing.Point(693, 21);
            this.cp_cStartDate.Name = "cp_cStartDate";
            this.cp_cStartDate.Size = new System.Drawing.Size(267, 26);
            this.cp_cStartDate.TabIndex = 8;
            this.cp_cStartDate.Value = new System.DateTime(2020, 4, 12, 15, 46, 28, 0);
            // 
            // cp_cStartDateLabel
            // 
            this.cp_cStartDateLabel.AutoSize = true;
            this.cp_cStartDateLabel.Font = new System.Drawing.Font("Courier New", 10F);
            this.cp_cStartDateLabel.Location = new System.Drawing.Point(562, 26);
            this.cp_cStartDateLabel.Name = "cp_cStartDateLabel";
            this.cp_cStartDateLabel.Size = new System.Drawing.Size(109, 20);
            this.cp_cStartDateLabel.TabIndex = 7;
            this.cp_cStartDateLabel.Text = "Start Date";
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(150, 50);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 100);
            this.panel1.TabIndex = 5;
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog_FileOk);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            this.openFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog_FileOk);
            // 
            // Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1063, 586);
            this.Controls.Add(this.panelSpradlin_createGoalPanel);
            this.Controls.Add(this.panelBraith);
            this.Controls.Add(this.panelBethely);
            this.Controls.Add(this.panelSpradlin_viewGoal);
            this.Controls.Add(this.panelSpradlin_mainPanel);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form";
            this.Text = "Tracking++";
            this.panelSpradlin_mainPanel.ResumeLayout(false);
            this.panelSpradlin_mainPanel.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panelSpradlin_viewGoal.ResumeLayout(false);
            this.panelSpradlin_viewGoal.PerformLayout();
            this.panelSpradlin_createGoalPanel.ResumeLayout(false);
            this.panelSpradlin_createGoalPanel.PerformLayout();
            this.cp_timedPanel.ResumeLayout(false);
            this.cp_timedPanel.PerformLayout();
            this.cp_stagedPanel.ResumeLayout(false);
            this.cp_stagedPanel.PerformLayout();
            this.cp_reminderPanel.ResumeLayout(false);
            this.cp_reminderPanel.PerformLayout();
            this.cp_customPanel.ResumeLayout(false);
            this.cp_customPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelSpradlin_mainPanel;
        private System.Windows.Forms.Panel panelSpradlin_viewGoal;
        private System.Windows.Forms.Panel panelBethely;
        private System.Windows.Forms.Panel panelBraith;
        private System.Windows.Forms.Panel panelSpradlin_createGoalPanel;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ListBox mp_goalList;
        private System.Windows.Forms.Label mp_goalListLbl;
        private System.Windows.Forms.Button mp_editGoalBtn;
        private System.Windows.Forms.Button mp_deleteGoalBtn;
        private System.Windows.Forms.Button mp_createNewGoalBtn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cp_goalTypeComboBox;
        private System.Windows.Forms.Panel cp_customPanel;
        private System.Windows.Forms.Panel cp_timedPanel;
        private System.Windows.Forms.Panel cp_stagedPanel;
        private System.Windows.Forms.TextBox cp_goalNameTextBox;
        private System.Windows.Forms.Label cp_goalNameLabel;
        private System.Windows.Forms.Label cp_goalTypeLabel;
        private System.Windows.Forms.Panel cp_reminderPanel;
        private System.Windows.Forms.Label cp_cStartDateLabel;
        private System.Windows.Forms.DateTimePicker cp_cStartDate;
        private System.Windows.Forms.Button cp_goBackBtn;
        private System.Windows.Forms.DateTimePicker cp_cEndDate;
        private System.Windows.Forms.Label cp_cEndDateLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cp_cRemoveTaskBtn;
        private System.Windows.Forms.Button cp_cAddTaskBtn;
        private System.Windows.Forms.ListBox cp_cTaskListBox;
        private System.Windows.Forms.CheckBox cp_cNoEndDateCheck;
        private System.Windows.Forms.Button cp_cCreateGoalBtn;
        private System.Windows.Forms.TextBox cp_cErrorMsgBox;
        private System.Windows.Forms.Label cp_tGoalDescriptionLabel;
        private System.Windows.Forms.TextBox cp_tGoalDescription;
        private System.Windows.Forms.TextBox cp_tErrorMsgBox;
        private System.Windows.Forms.Button cp_tCreateGoalBtn;
        private System.Windows.Forms.DateTimePicker cp_tEndDate;
        private System.Windows.Forms.Label cp_tEndDateLabel;
        private System.Windows.Forms.DateTimePicker cp_tStartDate;
        private System.Windows.Forms.Label cp_tStartDateLabel;
        private System.Windows.Forms.TextBox cp_sErrorMsgBox;
        private System.Windows.Forms.Button cp_sCreateGoalBtn;
        private System.Windows.Forms.Label cp_sTaskListLabel;
        private System.Windows.Forms.Button cp_sDeleteTaskBtn;
        private System.Windows.Forms.Button cp_sAddTaskBtn;
        private System.Windows.Forms.ListBox cp_sTaskListBox;
        private System.Windows.Forms.DateTimePicker cp_sEndDate;
        private System.Windows.Forms.Label cp_sEndDateLabel;
        private System.Windows.Forms.DateTimePicker cp_sStartDate;
        private System.Windows.Forms.Label cp_sStartDateLabel;
        private System.Windows.Forms.Label cp_rGoalDescriptionLabel;
        private System.Windows.Forms.TextBox cp_rGoalDescription;
        private System.Windows.Forms.TextBox cp_rErrorMsgBox;
        private System.Windows.Forms.Button cp_rCreateGoalBtn;
        private System.Windows.Forms.DateTimePicker cp_rStartDate;
        private System.Windows.Forms.Label cp_rStartDateLabel;
        private System.Windows.Forms.Label vp_goalStatus;
        private System.Windows.Forms.Label vp_goalStatusLabel;
        private System.Windows.Forms.Label vp_goalName;
        private System.Windows.Forms.Label vp_goalNameLabel;
        private System.Windows.Forms.Label vp_ms_checkBoxLabel;
        private System.Windows.Forms.CheckedListBox vp_ms_checkBox;
        private System.Windows.Forms.Button vp_goBackBtn;
        private System.Windows.Forms.Button vp_ss_goalCompletedBtn;
        private System.Windows.Forms.TextBox vp_ss_goalDescription;
        private System.Windows.Forms.Label vp_ss_goalDescriptionLabel;
        private System.Windows.Forms.Label vp_endDate;
        private System.Windows.Forms.Label vp_startDate;
        private System.Windows.Forms.Label vp_endDateLabel;
        private System.Windows.Forms.Label vp_startDateLabel;
        private System.Windows.Forms.Button cp_rEditGoalButton;
        private System.Windows.Forms.Button cp_goBackBtn2;
        private System.Windows.Forms.Button cp_tEditGoalButton;
        private System.Windows.Forms.Button cp_cEditGoalButton;
        private System.Windows.Forms.Button cp_sEditGoalButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
    }
}