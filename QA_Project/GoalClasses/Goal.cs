﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_Project
{
    [Serializable]
    // Abstract class that all goals are based from
    public abstract class Goal : IComparer<Goal>
    {
        private String name;
        private IBaseGoalType goalControl;
        public void SetName(String name)
        {
            this.name = name;
        }
        public String GetName()
        {
            return this.name;
        }

        public IBaseGoalType GetGoalControl()
        {
            return this.goalControl;
        }
        public void SetGoalControl(IBaseGoalType goalControl)
        {
            this.goalControl = goalControl;
        }

        public Boolean HasUnlimitedTime()
        {
            return (goalControl.GetEndDate().Equals(DateTime.MaxValue));
        }
        public String GetTimeLeftString()
        {
            // Creates Human-readable version of the current goal's status based on start date, end date, and completion status.
            if (goalControl.IsCompleted())
                return "Completed";
            if (goalControl.HasTimePassed())
                return "Past Due";
            if (!goalControl.IsActive())
            {
                var timeTill = goalControl.GetTimeTillActive();
                if (timeTill.TotalDays <= 1)
                {
                    if (timeTill.TotalDays < 1)
                    {
                        if (timeTill.TotalHours > 1)
                        {
                            // +1 takes into account 23hrs 59mins 
                            return ((int)timeTill.TotalHours + 1) + " Hours Till Active";
                        }
                        return timeTill.ToString(@"mm\:ss") + " Till Active"; ;
                    }
                    return "One Day Till Active";
                }
                return (int)(timeTill.TotalDays) + " Days Till Active";
            }
            if (HasUnlimitedTime())
                return "No Due Date";
            TimeSpan timeLeft = goalControl.GetTimeLeft();
            if (timeLeft.TotalDays <= 1)
            {
                if (timeLeft.TotalDays < 1)
                {
                    if (timeLeft.TotalHours > 1)
                    {
                        // +1 takes into account 23hrs 59mins 
                        return ((int)timeLeft.TotalHours + 1) + " Hours Left";
                    }
                    return timeLeft.ToString(@"mm\:ss"); ;
                }
                return "One Day Left";
            }
            return (int)(timeLeft.TotalDays) + " Days Left";
        }
        override public String ToString()
        {
            return String.Format("{0,-35} |{1,20}", GetName(), GetTimeLeftString());
        }
        public int Compare(Goal x, Goal y)
        {
            if (x.goalControl.IsCompleted() || y.goalControl.IsCompleted())
            {
                if (x.goalControl.IsCompleted() && y.goalControl.IsCompleted())
                    return 0;
                if (x.goalControl.IsCompleted())
                    return 1;
                return -1;
            }
            if (!x.goalControl.IsActive() || !y.goalControl.IsActive())
            {
                if (!x.goalControl.IsActive() && !y.goalControl.IsActive())
                    return 0;
                if (!x.goalControl.IsActive())
                    return 1;
                return -1;
            }
            return x.GetGoalControl().GetEndDate().CompareTo(y.GetGoalControl().GetEndDate());
        }
    }
}

