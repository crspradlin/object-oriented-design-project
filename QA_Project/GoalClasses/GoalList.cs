﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_Project.GoalClasses
{
    [Serializable]
    // Type of collection to sort goals as they are added to the list (Priority Queue for Goals)
    public class GoalList : List<Goal>
    {
        public new void Add(Goal goal)
        {
            base.Add(goal);
            base.Sort(goal.Compare);
        }
    }
}

