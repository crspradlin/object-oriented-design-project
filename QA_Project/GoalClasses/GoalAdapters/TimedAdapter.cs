﻿using QA_Project.GoalClasses.GoalTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_Project.GoalClasses.GoalAdapters
{
    class TimedAdapter : IAdapter
    {
        // Adapter used to convert any goal into a Timed Goal
        public Goal Convert(Goal goal)
        {
            if (goal.GetType() == Type.GetType("TimedGoal"))
            {
                return goal;
            }
            else
            {
                DateTime endDate = goal.GetGoalControl().GetEndDate();
                if(endDate == DateTime.MaxValue)
                {
                    endDate = goal.GetGoalControl().GetStartDate().AddDays(1);
                }
                var conversion = new TimedGoal(goal.GetName(), goal.GetGoalControl().GetStartDate(), endDate, goal.GetGoalControl().GetTasks().ElementAt(0).getDescription());
                if (goal.GetGoalControl().GetTasks().ElementAt(0).isDone())
                {
                    conversion.GetGoalControl().GetTasks().ElementAt(0).setDoneTo(true);
                }
                return conversion;
            }
        }
    }
}

