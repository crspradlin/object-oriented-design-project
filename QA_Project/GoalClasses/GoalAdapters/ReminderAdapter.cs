﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_Project.GoalClasses.GoalAdapters
{
    class ReminderAdapter : IAdapter
    {
        // Adapter used to convert any goal into a Reminder Goal
        public Goal Convert(Goal goal)
        { 
            if (goal.GetType() == Type.GetType("ReminderGoal"))
            {
                return goal;
            }
            else
            {
                var conversion = new ReminderGoal(goal.GetName(), goal.GetGoalControl().GetStartDate(), goal.GetGoalControl().GetTasks().ElementAt(0).getDescription());
                if (goal.GetGoalControl().GetTasks().ElementAt(0).isDone())
                {
                    conversion.GetGoalControl().GetTasks().ElementAt(0).setDoneTo(true);
                }
                return conversion;
            }
        }
        
    }
}
