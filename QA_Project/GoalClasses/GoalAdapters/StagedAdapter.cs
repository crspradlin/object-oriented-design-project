﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_Project.GoalClasses.GoalAdapters
{
    class StagedAdapter : IAdapter
    { 
        // Adapter to convert any goal into a Staged Goal
        public Goal Convert(Goal goal)
        {
            if (goal.GetType() == Type.GetType("StagedGoal"))
            {
                return goal;
            }
            else
            {
                DateTime endDate = goal.GetGoalControl().GetEndDate();
                if (goal.HasUnlimitedTime())
                {
                    endDate = goal.GetGoalControl().GetStartDate().AddDays(1);
                }
                List<String> stages = new List<string>();
                // Moving all tasks to the new Staged Goal
                foreach (Timer.Stage stage in goal.GetGoalControl().GetTasks())
                {
                    stages.Add(stage.getDescription());
                }
                var conversion = new StagedGoal(goal.GetName(), goal.GetGoalControl().GetStartDate(), endDate, stages);
                // Correlates all tasks with weather or not they were completed in the previous goal
                for (var i=0; i<conversion.GetGoalControl().GetTasks().Count; i++)
                {
                    if (goal.GetGoalControl().GetTasks().ElementAt(i).isDone())
                    {
                        conversion.GetGoalControl().GetTasks().ElementAt(i).setDoneTo(true);
                    }
                }
                return conversion;
            }
        }
    }
}

