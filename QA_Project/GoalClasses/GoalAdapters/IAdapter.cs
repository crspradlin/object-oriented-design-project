﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_Project.GoalClasses.GoalAdapters 
{
    public interface IAdapter
    {
        // Interface for all goal adapters
        Goal Convert(Goal goal);
    }
}

