﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_Project.GoalClasses.GoalAdapters
{
    class CustomAdapter : IAdapter
    {
        // Adapter to convert any goal into a Custom Goal
        public Goal Convert(Goal goal)
        {  
            if (goal.GetType() == Type.GetType("CustomGoal"))
            {
                return goal;
            }
            else
            {
                DateTime endDate = goal.GetGoalControl().GetEndDate();

                List<String> stages = new List<String>();

                var goalType = "StagedGoalType";
                // Need to move stages to the new Custom Goal
                foreach (Timer.Stage stage in goal.GetGoalControl().GetTasks())
                {
                    stages.Add(stage.getDescription());
                }
                if (stages.Count < 1)
                {
                    if (goal.HasUnlimitedTime())
                        goalType = "ReminderGoalType";
                    else
                        goalType = "TimedGoalType";
                }
                var conversion = new CustomGoal(goalType, goal.GetName(), goal.GetGoalControl().GetStartDate(), endDate, stages);
                // If tasks from the previous goal are completed, need to transfer that data to the new Custom Goal.
                for (var i = 0; i < conversion.GetGoalControl().GetTasks().Count; i++)
                {
                    if (goal.GetGoalControl().GetTasks().ElementAt(i).isDone())
                    {
                        conversion.GetGoalControl().GetTasks().ElementAt(i).setDoneTo(true);
                    }
                }
                return conversion;
            }
        }
    }
}
