﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_Project
{
    [Serializable]
    class ReminderGoal : Goal
    {
        // Type of goal, with a single task/stage and no end date
        public ReminderGoal(String name, DateTime startDate, String reminderDescription)
        {
            this.SetName(name);
            this.SetGoalControl(new ReminderGoalType(startDate, reminderDescription));
        }
    }
}
