﻿using QA_Project.GoalClasses.GoalTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_Project
{
    [Serializable]
    class CustomGoal : Goal
    {
        // Type of goal that determines what goal type to use automatically
        public CustomGoal(String goalType, String name, DateTime startDate, DateTime endDate, List<string> stageDescriptions)
        {
            this.SetName(name);
            if (goalType == "StagedGoalType")
                this.SetGoalControl(new StagedGoalType(startDate, endDate, stageDescriptions));
            else if (goalType == "ReminderGoalType")
                this.SetGoalControl(new ReminderGoalType(startDate, stageDescriptions.ElementAt(0)));
            else
                this.SetGoalControl(new TimedGoalType(startDate, endDate, stageDescriptions.ElementAt(0))); 
        }
    }
}
