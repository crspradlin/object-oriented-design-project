﻿using QA_Project.GoalClasses.GoalTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_Project
{
    [Serializable]
    class TimedGoal : Goal
    {
        // Goal with a single task and a start/end date
        public TimedGoal(String name, DateTime startDate, DateTime endDate, String goalDescription)
        {
            this.SetName(name);
            this.SetGoalControl(new TimedGoalType(startDate, endDate, goalDescription));
        }
    }
}
