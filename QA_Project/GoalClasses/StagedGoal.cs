﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_Project
{
    [Serializable]
    class StagedGoal : Goal
    {
        // Type of goal with multiple tasks/stages with start and end dates
        public StagedGoal(String name, DateTime startDate, DateTime endDate, List<String> stages)
        {
            this.SetName(name);
            this.SetGoalControl(new StagedGoalType(startDate, endDate, stages));
        }
    }
}
