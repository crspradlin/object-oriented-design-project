using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_Project.GoalClasses.GoalTypes
{
    [Serializable]
    class TimedGoalType : IBaseGoalType
    {
        // Goal Type for creating timed based goals
        private Timer timer;
        private String description;

        public TimedGoalType(DateTime startDate, DateTime endDate, String goalDescription)
        {
            // Can have only a single task, hence the single string param named goal Description
            var stages = new List<String>();
            stages.Add(goalDescription);
            description = goalDescription;
            this.timer = new Timer(startDate, endDate, stages);
        }

        public DateTime GetEndDate()
        {
            //Returns timer's end date
            return timer.getEndDate();
        }

        public void SetEndDate(DateTime date)
        {
            //Sets timer's end date
            timer.setEndDate(date);
        }

        public DateTime GetStartDate()
        {
            //Returns timer's start date
            return timer.getStartDate();
        }

        public void SetStartDate(DateTime date)
        {
            //Sets timer's start date
            timer.setStartDate(date);
        }

        public TimeSpan GetTimeLeft()
        {
            //Returns the amount of time left to complete goal
            return this.GetEndDate().Subtract(DateTime.Now);
        }

        public List<Timer.Stage> GetTasks()
        {
            return timer.getStages();
        }

        public Boolean IsCompleted()
        {
            //Returns true if the single stage has been completed
            var stage = timer.getStages().ElementAt(0);
            return stage.isDone();
        }

        public bool IsLate()
        {
            //Returns true if the goal is not completed AND past due
            if (HasTimePassed() && !IsCompleted())
                return true;
            return false;
        }

        public bool HasTimePassed()
        {
            //Returns true if end date has passed
            return GetTimeLeft() < new TimeSpan(0);
        }

        public bool IsActive()
        {
            //Returns true if not completed AND end date has not passed AND start date has passed
            var durationSinceStart = DateTime.Now.Subtract(GetStartDate());
            if (durationSinceStart > new TimeSpan(0) && !IsCompleted())
                return true;
            return false;
        }
        public TimeSpan GetTimeTillActive()
        {
            if (IsActive())
            {
                return new TimeSpan(0);
            }
            return (GetStartDate().Subtract(DateTime.Now));
        }
    }
}
