using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_Project
{
    [Serializable]
    class StagedGoalType : IBaseGoalType
    {
        // Creates the goal type for staged goals
        private Timer timer;
        private List<String> stages;

        public StagedGoalType(DateTime startDate, DateTime endDate, List<String> stageDescriptions)
        {
            // Can include multiple tasks due to the list of stageDescriptions
            this.timer = new Timer(startDate, endDate, stageDescriptions);
            stages = stageDescriptions;
        }

        public DateTime GetEndDate()
        {
            //Returns timer's end date
            return timer.getEndDate();
        }

        public void SetEndDate(DateTime date)
        {
            //Sets timer's end date
            timer.setEndDate(date);
        }

        public DateTime GetStartDate()
        {
            //Returns timer's start date
            return timer.getStartDate();
        }

        public void SetStartDate(DateTime date)
        {
            //Sets timer's start date
            timer.setStartDate(date);
        }

        public TimeSpan GetTimeLeft()
        {
            //Returns the amount of time left to complete goal
            return timer.getEndDate().Subtract(DateTime.Now);
        }

        public List<Timer.Stage> GetTasks()
        {
            return timer.getStages();
        }

        public Boolean IsCompleted()
        {
            //Returns true if all stages are complete
            var stages = timer.getStages();
            foreach (Timer.Stage stage in stages)
            {
                if (!stage.isDone()) return false;
            }
            return true;
        }

        public bool IsLate()
        {
            //Returns true if the goal is not completed AND past due
            if (HasTimePassed() && !IsCompleted())
                return true;
            return false;
        }

        public bool HasTimePassed()
        {
            //Returns true if end date has passed
            return GetTimeLeft() < new TimeSpan(0);
        }

        public bool IsActive()
        {
            //Returns true if not completed AND end date has not passed AND start date has passed
            var durationSinceStart = DateTime.Now.Subtract(GetStartDate());
            if(durationSinceStart > new TimeSpan(0) && !IsCompleted())
                return true;
            return false;
        }
        public TimeSpan GetTimeTillActive()
        {
            if (IsActive())
            {
                return new TimeSpan(0);
            }
            return (GetStartDate().Subtract(DateTime.Now));
        }

        public List<String> GetStages()
        {
            return stages;
        }
    }
}
