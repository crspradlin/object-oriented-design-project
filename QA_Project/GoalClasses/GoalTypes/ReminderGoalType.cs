﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_Project
{
    [Serializable]
    class ReminderGoalType : IBaseGoalType
    {
        // Goal type for reminder based goals
        private Timer timer;

        public ReminderGoalType(DateTime startDate, String reminderDescription)
        {
            // Having the constructor only accept a single string prevents muliple stages/tasks within a reminder goal
            var stages = new List<String>();
            stages.Add(reminderDescription);
            this.timer = new Timer(startDate, DateTime.MaxValue, stages);
        }

        public DateTime GetEndDate()
        {
            // Returns timer's end date
            return timer.getEndDate();
        }

        public void SetEndDate(DateTime date)
        {
            // Sets timer's end date
            timer.setEndDate(date);
        }

        public DateTime GetStartDate()
        {
            // Returns timer's start date
            return timer.getStartDate();
        }

        public void SetStartDate(DateTime date)
        {
            // Sets timer's start date
            timer.setStartDate(date);
        }

        public TimeSpan GetTimeLeft()
        {
            // Returns the amount of time left to complete goal
            return timer.getEndDate().Subtract(DateTime.Now);
        }

        public List<Timer.Stage> GetTasks()
        {
            return timer.getStages();
        }

        public Boolean IsCompleted()
        {
            // Returns true if all stages are complete
            var stage = timer.getStages().ElementAt(0);
            return stage.isDone();
        }

        public bool IsLate()
        {
            // Reminders cannot be late;
            return false;
        }

        public bool HasTimePassed()
        {
            // Returns true if end date has passed
            return false;
        }

        public bool IsActive()
        {
            // Returns true if not completed AND end date has not passed AND start date has passed
            var durationSinceStart = DateTime.Now.Subtract(GetStartDate());
            if (durationSinceStart > new TimeSpan(0) && !IsCompleted())
                return true;
            return false;
        }

        public TimeSpan GetTimeTillActive()
        {
            if (IsActive())
            {
                return new TimeSpan(0);
            }
            return (GetStartDate().Subtract(DateTime.Now));
        }
    }
}
