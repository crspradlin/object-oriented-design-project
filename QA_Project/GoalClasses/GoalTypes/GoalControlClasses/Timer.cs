﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_Project
{
    [Serializable]
    public class Timer
    {
        [Serializable]
        public class Stage
        {
            // Class of Timer to keep track of the tasks that need to be done for the goal 
            private Boolean done;
            private String description;

            public Stage(string description)
            {
                this.description = description;
                this.done = false;
            }

            public Stage(string description, Boolean initial)
            {
                this.description = description;
                this.done = initial;
            }

            public void setDoneTo(Boolean value)
            {
                this.done = value;
            }
            public Boolean isDone()
            {
                return this.done;
            }
            public String getDescription()
            {
                return this.description;
            }
            public void setDescription(String description)
            {
                this.description = description;
            }
            public override String ToString()
            {
                return this.getDescription();
            }
        }
        // Each goal has a list of stages that need to be completed (Reminders and Timers have only a single stage to complete)
        private List<Stage> stages;
        private DateTime endDate;
        private DateTime startDate;
        public Timer(DateTime startDate, DateTime endDate, List<String> descriptions)
        {
            this.stages = new List<Stage>();
            this.startDate = startDate;
            this.endDate = endDate;
            foreach (String description in descriptions)
            {
                this.stages.Add(new Stage(description));
            }
        }
        public List<Stage> setStageCompleteTo(Stage stage, Boolean value)
        {
            stage.setDoneTo(value);
            return this.stages;
        }
        public List<Stage> getStages()
        {
            return this.stages;
        }
        public DateTime getEndDate()
        {
            return this.endDate;
        }
        public void setEndDate(DateTime endDate)
        {
            this.endDate = endDate;
        }
        public DateTime getStartDate()
        {
            return this.startDate;
        }
        public void setStartDate(DateTime startDate)
        {
            this.startDate = startDate;
        }
    }
}