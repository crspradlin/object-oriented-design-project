﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_Project
{
    // Interface for all goal types (aka. goal controls)
    public interface IBaseGoalType
    {
        DateTime GetStartDate();
        void SetStartDate(DateTime date);
        Boolean IsCompleted();
        Boolean IsActive();
        TimeSpan GetTimeTillActive();
        DateTime GetEndDate();
        void SetEndDate(DateTime date);
        Boolean IsLate();
        TimeSpan GetTimeLeft();
        Boolean HasTimePassed();
        List<Timer.Stage> GetTasks();
    }
}