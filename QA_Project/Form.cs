﻿﻿using QA_Project.GoalClasses;
using QA_Project.PanelObserverClasses;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using QA_Project.PanelClasses;
using QA_Project.FileControlClasses;
using System.IO;

namespace QA_Project
{
    public partial class Form : System.Windows.Forms.Form
    {
        private AppDataFileController appData;
        private GoalList goals;
        private TimerSubject timerSubject;
        public Form()
        {
            // Draws GUI, Window and corresponding components for the Application
            this.goals = new GoalList();
            InitializeComponent();
            InitializeTimer();
            InitializeGoals();
        }

        private void setToVisible(Panel panel)
        {
            // Used to set the correct panel visible to the user
            this.panelBethely.Visible = false;
            this.panelSpradlin_mainPanel.Visible = false;
            this.panelSpradlin_createGoalPanel.Visible = false;
            this.panelBraith.Visible = false;
            this.panelSpradlin_viewGoal.Visible = false;
            panel.Visible = true;
        }

        private void InitializeGoals()
        {
            // Loads the goals saved to app data into the application
            appData = AppDataFileController.getInstance();
            var pre_goals = appData.loadFromAppData();
            this.goals.Clear();
            foreach(Goal goal in pre_goals)
            {
                this.goals.Add(goal);
            }
        }

        private void InitializeTimer()
        {
            //subject
            this.timerSubject = new TimerSubject(this.goals);
            //attach observers
            this.spradlinPanelObserver = new SpradlinPanelObserver(this.panelSpradlin_mainPanel);
            timerSubject.Attach(spradlinPanelObserver);
            this.spradlinGoalViewPanelObserver = new ViewGoalPanelObserver(this.panelSpradlin_viewGoal);
            timerSubject.Attach(spradlinGoalViewPanelObserver);
        }
        private void timer_Tick(object sender, EventArgs e)
        {
            //Notify all timerObserver objects that a second has passed. (Used to update panels that require timely updates)
            if (this.mp_goalList.SelectedIndex != -1) {
                this.spradlinGoalViewPanelObserver.SetGoal(this.goals.ElementAt(this.mp_goalList.SelectedIndex));
             }
            this.timerSubject.Notify();
        }
    }
}

