using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using QA_Project.FileControlClasses;
using QA_Project.GoalClasses.GoalAdapters;
using QA_Project.PanelClasses;

namespace QA_Project
{
    public partial class Form : System.Windows.Forms.Form
    {
        //////////////////////////////////////////////////////////////////
        //mainPanel

        private SpradlinPanelObserver spradlinPanelObserver;
        private PanelObserverClasses.ViewGoalPanelObserver spradlinGoalViewPanelObserver;
        private void mp_createNewGoalBtn_Click(object sender, EventArgs e)
        {
            this.setToVisible(this.panelSpradlin_createGoalPanel);
        }

        public void UpdateMainPanelGoalList()
        {
            this.mp_goalList.BeginUpdate();
            if (this.goals.Count > this.mp_goalList.Items.Count)
            {
                var difference = this.goals.Count - this.mp_goalList.Items.Count;
                for (int i = 0; i < difference; i++)
                {
                    this.mp_goalList.Items.Add(this.goals.ElementAt(i + this.mp_goalList.Items.Count));
                }
            }
            this.mp_goalList.EndUpdate();
        }

        //////////////////////////////////////////////////////////////////
        //createGoalPanel

        private void cp_setVisible(Panel panel)
        {
            this.cp_customPanel.Visible = false;
            this.cp_timedPanel.Visible = false;
            this.cp_stagedPanel.Visible = false;
            this.cp_reminderPanel.Visible = false;

            panel.Visible = true;
        }
        private void cp_clearFields()
        {
            //Clear all fields
            this.cp_goalNameTextBox.Text = "";
            this.cp_cTaskListBox.Items.Clear();
            this.cp_sTaskListBox.Items.Clear();
            this.cp_tGoalDescription.Text = "";
            this.cp_rGoalDescription.Text = "";
            this.cp_cErrorMsgBox.Text = "";
            this.cp_sErrorMsgBox.Text = "";
            this.cp_tErrorMsgBox.Text = "";
            this.cp_rErrorMsgBox.Text = "";
            this.cp_cNoEndDateCheck.Checked = false;
        }
        private void cp_updateDates()
        {
            this.cp_cEndDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, 0);
            this.cp_tEndDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, 0);
            this.cp_sEndDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, 0);
            //this.cp_rEndDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, 0);

            this.cp_cStartDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, 0);
            this.cp_tStartDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, 0);
            this.cp_sStartDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, 0);
            this.cp_rStartDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, 0);

        }
        private void cp_goalTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cp_goBackBtn2.Visible)
            {
                var editGoal = goals.ElementAt(mp_goalList.SelectedIndex);
                if (cp_goalTypeComboBox.SelectedItem.ToString() == "Timed")
                    editGoal = new TimedAdapter().Convert(editGoal);
                else if (cp_goalTypeComboBox.SelectedItem.ToString() == "Reminder")
                    editGoal = new ReminderAdapter().Convert(editGoal);
                else if (cp_goalTypeComboBox.SelectedItem.ToString() == "Multi Part")
                    editGoal = new StagedAdapter().Convert(editGoal);
                else
                    editGoal = new CustomAdapter().Convert(editGoal);
                cp_mp_editGoal(editGoal);
            }
            else
            {
                if (cp_goalTypeComboBox.SelectedItem.ToString() == "Timed")
                    this.cp_setVisible(this.cp_timedPanel);
                else if (cp_goalTypeComboBox.SelectedItem.ToString() == "Reminder")
                    this.cp_setVisible(this.cp_reminderPanel);
                else if (cp_goalTypeComboBox.SelectedItem.ToString() == "Multi Part")
                    this.cp_setVisible(this.cp_stagedPanel);
                else
                    this.cp_setVisible(this.cp_customPanel);
                this.cp_updateDates();
            }
            
        }
        private void cp_goBackBtn_Click(object sender, EventArgs e)
        {
            this.setToVisible(this.panelSpradlin_mainPanel);
        }
        private String cp_validateBaseForm()
        {
            if (this.cp_goalNameTextBox.Text.Length > 35)
                return "Goal Name must be less than 35 characters!";
            if (this.cp_goalNameTextBox.Text == "")
                return "You must have a Goal Name!";
            return "";
        }
        //Custom Goal
        private void cp_cAddTaskBtn_Click(object sender, EventArgs e)
        {
            string input = Interaction.InputBox("What is the description for your new task?",
                       "New Task",
                       "",
                       0,
                       0);
            this.cp_cTaskListBox.Items.Add(input);
        }
        private void cp_cTaskListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cp_cRemoveTaskBtn.Enabled = true;
        }
        private void cp_cRemoveTaskBtn_Click(object sender, EventArgs e)
        {
            this.cp_cTaskListBox.Items.RemoveAt(this.cp_cTaskListBox.SelectedIndex);
        }
        private String cp_cValidateForm()
        {
            if (cp_validateBaseForm() != "")
                return cp_validateBaseForm();
            if (cp_cStartDate.Value > cp_cEndDate.Value)
                return "Invalid date combination, start date is past the end date!";
            if (cp_cTaskListBox.Items.Count < 1)
                return "Must have at least one task to complete!";
            return "";
        }
        private void cp_cNoEndDateCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (cp_cNoEndDateCheck.Checked)
                this.cp_cEndDate.Enabled = false;
            else
                this.cp_cEndDate.Enabled = true;
        }
        private void cp_cCreateGoalBtn_Click(object sender, EventArgs e)
        {
            var validation = cp_cValidateForm();
            if (validation != "")
            {
                this.cp_cErrorMsgBox.Text = validation;
            }
            else
            {
                var tasks = new List<String>();
                var goalType = "StagedGoalType";
                foreach (String item in this.cp_cTaskListBox.Items)
                {
                    tasks.Add(item);
                }
                if (tasks.Count < 1)
                {
                    if (this.cp_cNoEndDateCheck.Checked)
                        goalType = "ReminderGoalType";
                    else
                        goalType = "TimedGoalType";
                }
                var newGoal = new CustomGoal(goalType, this.cp_goalNameTextBox.Text, this.cp_cStartDate.Value, this.cp_cEndDate.Value, tasks);
                this.goals.Add(newGoal);
                this.appData.saveToAppData(this.goals);
                this.setToVisible(this.panelSpradlin_mainPanel);
                this.cp_clearFields();
            }
        }
        //Timed Goal
        private String cp_tValidateForm()
        {
            if(cp_validateBaseForm() != "")
                return cp_validateBaseForm();
            if(cp_tStartDate.Value > cp_tEndDate.Value)
                return "Invalid date combination, start date is past the end date!";
            if (cp_tGoalDescription.Text == "")
                return "You must have a goal description for a timed goal!";
            return "";
        }
        private void cp_tCreateGoalBtn_Click(object sender, EventArgs e)
        {
            var validation = cp_tValidateForm();
            if (validation != "")
            {
                this.cp_tErrorMsgBox.Text = validation;
            }
            else
            {
                var newGoal = new TimedGoal(this.cp_goalNameTextBox.Text, this.cp_tStartDate.Value, this.cp_tEndDate.Value, this.cp_tGoalDescription.Text);
                this.goals.Add(newGoal);
                this.appData.saveToAppData(this.goals);
                this.setToVisible(this.panelSpradlin_mainPanel);
                this.cp_clearFields();
            }
        }
        //Reminder Goal
        private String cp_rValidateForm()
        {
            if (cp_validateBaseForm() != "")
                return cp_validateBaseForm();
            if (cp_rGoalDescription.Text == "")
                return "You must have a goal description for a timed goal!";
            return "";
        }
        private void cp_rCreateGoalBtn_Click(object sender, EventArgs e)
        {
            var validation = cp_rValidateForm();
            if (validation != "")
            {
                this.cp_rErrorMsgBox.Text = validation;
            }
            else
            {
                var newGoal = new ReminderGoal(this.cp_goalNameTextBox.Text, this.cp_rStartDate.Value, this.cp_rGoalDescription.Text);
                this.goals.Add(newGoal);
                this.appData.saveToAppData(this.goals);
                this.setToVisible(this.panelSpradlin_mainPanel);
                this.cp_clearFields();
            }
        }
        //Staged Goal
        private String cp_sValidateForm()
        {
            if (cp_validateBaseForm() != "")
                return cp_validateBaseForm();
            if (cp_sStartDate.Value > cp_sEndDate.Value)
                return "Invalid date combination, start date is past the end date!";
            if (cp_sTaskListBox.Items.Count < 1)
                return "Must have at least one task to complete!";
            return "";
        }
        private void cp_sCreateGoalBtn_Click(object sender, EventArgs e)
        {
            var validation = cp_sValidateForm();
            if (validation != "")
            {
                this.cp_sErrorMsgBox.Text = validation;
            }
            else
            {
                var tasks = new List<String>();
                foreach (String item in this.cp_sTaskListBox.Items)
                {
                    tasks.Add(item);
                }
                var newGoal = new StagedGoal(this.cp_goalNameTextBox.Text, this.cp_sStartDate.Value, this.cp_sEndDate.Value, tasks);
                this.goals.Add(newGoal);
                this.appData.saveToAppData(this.goals);
                this.setToVisible(this.panelSpradlin_mainPanel);
                this.cp_clearFields();
            }
        }
        private void cp_sAddTaskBtn_Click(object sender, EventArgs e)
        {
            string input = Interaction.InputBox("What is the description for your new task?",
                       "New Task",
                       "",
                       0,
                       0);
            this.cp_sTaskListBox.Items.Add(input);
        }
        private void cp_sDeleteTaskBtn_Click(object sender, EventArgs e)
        {
            this.cp_sTaskListBox.Items.RemoveAt(this.cp_sTaskListBox.SelectedIndex);
        }

        //////////////////////////////////////////////////////////////////
        //viewGoalPanel

        private void mp_goalList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.goals.Count > 0 && this.mp_goalList.SelectedIndex > -1)
            {
                //get selected type and display message box according to that type...
                var goal = this.goals.ElementAt(this.mp_goalList.SelectedIndex);
                this.vp_startDate.Text = goal.GetGoalControl().GetStartDate().ToString("MM/dd/yyyy hh:mm tt");
                this.vp_goalName.Text = goal.GetName();
                this.vp_endDate.Text = goal.GetGoalControl().GetEndDate().ToString("MM/dd/yyyy hh:mm tt");
                this.vp_goalStatus.Text = goal.GetTimeLeftString();
                if (goal.GetGoalControl().GetEndDate() == DateTime.MaxValue)
                {
                    this.vp_endDate.Text = "No End Date";
                }
                if (goal.GetType().Name == "StagedGoal" || goal.GetGoalControl().GetTasks().Count>1)
                {
                    this.setViewMutiGoalPanelsVisible(true);
                    this.vp_ms_checkBox.Items.Clear();
                    for(var i=0; i < goal.GetGoalControl().GetTasks().Count; i++)
                    {
                        var task = goal.GetGoalControl().GetTasks().ElementAt(i);
                        this.vp_ms_checkBox.Items.Add(task);
                        if (task.isDone())
                        {
                            this.vp_ms_checkBox.SetItemChecked(i, true);
                        }
                    }
                }
                else
                {
                    this.setViewMutiGoalPanelsVisible(false);
                    if (goal.GetGoalControl().GetTasks().Count > 0)
                        this.vp_ss_goalDescription.Text = goal.GetGoalControl().GetTasks().ElementAt(0).getDescription();
                    else
                        this.vp_ss_goalDescription.Text = "No Description";
                }
                this.spradlinGoalViewPanelObserver.SetGoal(this.goals.ElementAt(this.mp_goalList.SelectedIndex));
                this.setToVisible(this.panelSpradlin_viewGoal);
            }
        }
        private void setViewMutiGoalPanelsVisible(Boolean value)
        {
            if (value)
            {
                this.vp_ms_checkBox.Visible = true;
                this.vp_ms_checkBoxLabel.Visible = true;
                this.vp_ss_goalCompletedBtn.Visible = false;
                this.vp_ss_goalDescription.Visible = false;
                this.vp_ss_goalDescriptionLabel.Visible = false;
            }
            else
            {
                this.vp_ms_checkBox.Visible = false;
                this.vp_ms_checkBoxLabel.Visible = false;
                this.vp_ss_goalCompletedBtn.Visible = true;
                this.vp_ss_goalDescription.Visible = true;
                this.vp_ss_goalDescriptionLabel.Visible = true;
            }
        }

        private void vp_goBackBtn_Click(object sender, EventArgs e)
        {
            this.setToVisible(this.panelSpradlin_mainPanel);
            this.appData.saveToAppData(this.goals);
        }

        private void vp_ss_goalCompletedBtn_Click(object sender, EventArgs e)
        {
            //make it to where the button can be uncompleted
            var goal = this.spradlinGoalViewPanelObserver.GetGoal();
            goal.GetGoalControl().GetTasks().ElementAt(0).setDoneTo(true);
        }

        private void vp_ms_checkBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var goal = this.spradlinGoalViewPanelObserver.GetGoal();
            var tasks = goal.GetGoalControl().GetTasks();
            foreach (var task in tasks)
            {
                if (vp_ms_checkBox.CheckedItems.Contains(task))
                    task.setDoneTo(true);
                else
                    task.setDoneTo(false);
            }
        }

        //////////////////////////////////////////////////////////////////
        //fileSaveAndOpenDialogHandlers

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.saveFileDialog.Filter = "Goal Files | *.goal";
            this.saveFileDialog.Title = "Save Goal";
            this.saveFileDialog.DefaultExt = "*.goal";
            this.saveFileDialog.ShowDialog();
        }

        private void saveFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            String path = this.saveFileDialog.FileName;
            GoalWriter gw = new GoalWriter(path);
            if (File.Exists(path))
            {
                DialogResult dialogResult = MessageBox.Show("Do you want to add to the file? (yes to add, no to over write).", "Add to the file?", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    gw.writeGoal(this.goals.ElementAt(this.mp_goalList.SelectedIndex), false);
                }
                else if (dialogResult == DialogResult.No)
                {
                    gw.writeGoal(this.goals.ElementAt(this.mp_goalList.SelectedIndex), true);
                }
                else
                {
                    MessageBox.Show("Save has be canceled.", "Add to the file?", MessageBoxButtons.OK);
                }
            }
            else
            {
                gw.writeGoal(this.goals.ElementAt(this.mp_goalList.SelectedIndex), true);
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.openFileDialog.Filter = "Goal Files | *.goal";
            this.openFileDialog.Title = "Save Goal";
            this.openFileDialog.DefaultExt = "*.goal";
            this.openFileDialog.ShowDialog();
        }

        private void openFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            String path = this.openFileDialog.FileName;
            GoalReader gr = new GoalReader(path);
            var newGoals = gr.ReadGoals();
            foreach (Goal goal in newGoals)
            {
                this.goals.Add(goal);
            }
        }
    }
}
