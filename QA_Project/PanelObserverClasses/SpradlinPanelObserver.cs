﻿using QA_Project.GoalClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_Project.PanelClasses
{
    class SpradlinPanelObserver : ITimerObserver
    {
        // Observer used to update the main panel's list of goals and their respective end dates
        private System.Windows.Forms.Panel panel;

        public SpradlinPanelObserver(System.Windows.Forms.Panel panel)
        {
            this.panel = panel;
        }
        void ITimerObserver.ObserverUpdate(GoalList goals)
        {
            System.Windows.Forms.ListBox listBox = (System.Windows.Forms.ListBox) panel.Controls.Find("mp_goalList", true)[0];
            
            // Update list of goals' status within the goal list of the main panel
            listBox.BeginUpdate();
            if(listBox.Items.Count == goals.Count)
            {
                for(int i=0; i < listBox.Items.Count; i++)
                {
                    listBox.Items[i] = goals.ElementAt(i);
                }
            } 
            else
            {
                for(int i=0; i<goals.Count; i++)
                {
                    if (listBox.Items.Count - 1 < i)
                    {
                        listBox.Items.Add(goals.ElementAt(i));
                    }
                    else
                    {
                        listBox.Items[i] = goals.ElementAt(i);
                    }
                }
            }
            listBox.EndUpdate();
        }

    }
}
