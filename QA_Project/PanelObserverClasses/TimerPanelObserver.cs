﻿using QA_Project.GoalClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QA_Project
{
    public interface ITimerObserver
    {
        // Observer for update call
        void ObserverUpdate(GoalList goals);
    }

    public interface ISubject
    {
        // Attach an observer to the subject.
        void Attach(ITimerObserver observer);

        // Detach an observer from the subject.
        void Detach(ITimerObserver observer);

        // Notify all observers about an event.
        void Notify();
    }

    // Timer Panels, class to hold all panels that need to be updated at each timer tick
    public class TimerSubject : ISubject
    {
        private GoalList goals;
        public TimerSubject(GoalList goals)
        {
            this.goals = goals;
        }

        private List<ITimerObserver> _observers = new List<ITimerObserver>();

        // The subscription management methods.
        public void Attach(ITimerObserver observer)
        {
            this._observers.Add(observer);
        }

        public void Detach(ITimerObserver observer)
        {
            this._observers.Remove(observer);
        }

        // Trigger an update in each subscriber.
        public void Notify()
        {
            foreach (var observer in _observers)
            {
                observer.ObserverUpdate(this.goals);
            }
        }
    }
}
