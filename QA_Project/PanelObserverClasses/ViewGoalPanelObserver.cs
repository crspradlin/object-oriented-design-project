﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QA_Project.GoalClasses;
using QA_Project.PanelObserverClasses;

namespace QA_Project.PanelObserverClasses
{
    class ViewGoalPanelObserver : ITimerObserver
    {
        // Observer used to update the view panel's goal status on the current goal being viewed
        Panel panel;
        Goal goal;

        private GoalList tempGoalList;

        public ViewGoalPanelObserver(Panel panel)
        {
            this.goal = null;
            this.panel = panel;
        }

        public void SetGoal(Goal goal)
        {
            this.goal = goal;
        }

        public Goal GetGoal()
        {
            return this.goal;
        }

        public void ObserverUpdate(GoalList goals)
        {
            if (this.goal != null)
            {
                var statusBox = (System.Windows.Forms.Label)panel.Controls.Find("vp_goalStatus", true)[0];
                statusBox.Text = this.goal.GetTimeLeftString();
            }
        }
    }
}
